
#include <cstring>
#include <gtest/gtest.h>
#include "cpp/apply.h"
#include "cpp/util.h"

#define AMACRO(x) PASTE(a, x)
TEST(CppApply, apply_constructs_repeated_macro_invocation) {

    const char * expected = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    const char * actual = STRINGIZE(APPLYL(0, 127, (PASTE, (_1, _2)), (a, a)));

    ASSERT_STREQ(expected, actual);
    ASSERT_EQ(128ul, strlen(actual)); // 127 + 1 to account for the seed value.
}

