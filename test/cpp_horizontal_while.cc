
#include <gtest/gtest.h>
#include "cpp/horizontal/while.h"
#include "cpp/util.h"
#include "cpp/arithmetic.h"

#define PASSTHROUGH(a) a
#define SEMICOLON() ;

/**
 * @todo need more tests for while...
 */
TEST(CppRepeat, while_iterates) {

    ASSERT_STREQ("", STRINGIZE(HORIZONTAL_WHILE(ISTRUE, (_1), PASSTHROUGH, (_1), COMMA,)));
    ASSERT_STREQ("a", STRINGIZE(HORIZONTAL_WHILE(ISTRUE, (_1), PASSTHROUGH, (_1), COMMA, a)));
    ASSERT_STREQ(
        "1,2,3,4,5",
        STRINGIZE(HORIZONTAL_WHILE(ISTRUE, (_1), PASSTHROUGH, (_1), COMMA, 1, 2, 3, 4, 5, 0, 10, 2, 30, 25)));

    ASSERT_STREQ(
        "1234567891011121314151617181920",
        STRINGIZE(HORIZONTAL_WHILE(ISTRUE, (_1), ADD, (_1, _2), BLANK, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 2, 3, 4, 6, 4, )));

    ASSERT_STREQ(
        "1;2;3;4;5;6;7;8;a;b;c;d",
        STRINGIZE(HORIZONTAL_WHILE(ISTRUE, (_1), PASSTHROUGH, (_1), SEMICOLON, 1, 2, 3, 4, 5, 6, 7, 8, a, b, c, d)));

}
