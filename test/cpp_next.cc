
#include <gtest/gtest.h>
#include "cpp/integer.h"

TEST(CppNext, next_increments_naturals_from_0_to_511) {

    ASSERT_EQ(1, NEXT(0));
    ASSERT_EQ(10, NEXT(9));
    ASSERT_EQ(11, NEXT(10));
    ASSERT_EQ(100, NEXT(99));
    ASSERT_EQ(101, NEXT(100));
    ASSERT_EQ(200, NEXT(199));
    ASSERT_EQ(201, NEXT(200));
    ASSERT_EQ(300, NEXT(299));
    ASSERT_EQ(301, NEXT(300));
    ASSERT_EQ(400, NEXT(399));
    ASSERT_EQ(401, NEXT(400));
    ASSERT_EQ(500, NEXT(499));
    ASSERT_EQ(501, NEXT(500));
    ASSERT_EQ(512, NEXT(511));
}

TEST(CppNext, next_is_transitive) {
    ASSERT_EQ(4, NEXT(NEXT(NEXT(NEXT(0)))));
    ASSERT_EQ(512, NEXT(NEXT(NEXT(NEXT(NEXT(NEXT(NEXT(NEXT(504)))))))));
}

TEST(CppNext, next_yeilds_single_token) {
    ASSERT_STREQ("4", STRINGIZE(NEXT(NEXT(NEXT(NEXT(0))))));
}

