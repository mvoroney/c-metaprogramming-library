
#include <gtest/gtest.h>
#include "cpp/util.h"
#include "cpp/arithmetic.h"
#include "cpp/list/generate.h"
#include "cpp/list/fold.h"

#define ASSOCIATE(a, b) (a,b)

TEST(CppList, list_right_fold_yields_single_value) {
    ASSERT_EQ(0, LIST_RIGHT_FOLD(PASTE, (_1, _2), 0));
    
    ASSERT_EQ(
        123456789, 
        LIST_RIGHT_FOLD(PASTE, (_1, _2), 1, 2, 3, 4, 5, 6, 7, 8, 9));

    ASSERT_EQ(
        128,
        LIST_RIGHT_FOLD(ADD, (_1, _2), 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 8));

    ASSERT_EQ(
        120,
        LIST_RIGHT_FOLD(ADD, (_1, _2), LIST_GENERATE(16)));
}

TEST(CppList, list_reverse_reverses) {
    ASSERT_STREQ("", STRINGIZE(LIST_REVERSEL(0, COMMA,)));
    ASSERT_STREQ("1", STRINGIZE(LIST_REVERSEL(0, COMMA, 1)));
    ASSERT_STREQ(
        "9,8,7,6,5,4,3,2,1", 
        STRINGIZE(LIST_REVERSEL(0, COMMA, 1, 2, 3, 4, 5, 6, 7, 8, 9)));
}


TEST(CppList, list_left_fold_yields_single_value) {
    ASSERT_EQ(0, LIST_LEFT_FOLD(PASTE, (_1, _2), 0));
    
    ASSERT_EQ(
        123456789, 
        LIST_LEFT_FOLD(PASTE, (_1, _2), 1, 2, 3, 4, 5, 6, 7, 8, 9));

    ASSERT_EQ(
        128,
        LIST_LEFT_FOLD(ADD, (_1, _2), 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 8));

    ASSERT_EQ(
        120,
        LIST_LEFT_FOLD(ADD, (_1, _2), LIST_GENERATE(16)));
}

TEST(CppList, list_folds_associate_properly) {

    ASSERT_STREQ(
        "((((((((1,2),3),4),5),6),7),8),9)",
        STRINGIZE(LIST_LEFT_FOLD(ASSOCIATE, (_1, _2), 1, 2, 3, 4, 5, 6, 7, 8, 9)));

    ASSERT_STREQ(
        "(1,(2,(3,(4,(5,(6,(7,(8,9))))))))",
        STRINGIZE(LIST_RIGHT_FOLD(ASSOCIATE, (_1, _2), 1, 2, 3, 4, 5, 6, 7, 8, 9)));
}

TEST(CppList, list_reverse_accepts_parenthetical_elements) {
    ASSERT_STREQ("", STRINGIZE(LIST_REVERSEL(0, COMMA, )));
    ASSERT_STREQ("", STRINGIZE(LIST_REVERSEL(0, COMMA, ())));
    ASSERT_STREQ("()", STRINGIZE(LIST_REVERSEL(0, COMMA, (()))));
    ASSERT_STREQ("0", STRINGIZE(LIST_REVERSEL(0, COMMA, (0))));
    ASSERT_STREQ("(0)", STRINGIZE(LIST_REVERSEL(0, COMMA, ((0)))));
    ASSERT_STREQ("(),(c),(b),(a)", STRINGIZE(LIST_REVERSEL(0, COMMA, (a), (b), (c), ())));
}


