
#include <gtest/gtest.h>
#include "type/util.h"
#include "type/condition.h"

struct my_type {
    typedef my_type my_personal_typedef;    
};

template<typename T>
struct my_holder {
    typedef typename T::my_personal_typedef type;
};

const int success = 1;
const int failure = 2;

template<typename R = int>
typename type::condition::expand<true, R>::type fint() {
    return success;
}

template<typename R = int>
typename type::condition::expand<false, R>::type fint() {
    return failure;
}

TEST(TypeConditionExpand, forwards_true_type) {
    const bool result = type::eq<
        type::condition::expand<true, my_type>::type,
        my_type
    >::value;

    ASSERT_TRUE(result);
}

TEST(TypeConditionExpand, forwards_false_type) {
    const bool result = type::eq<
        type::condition::expand<false, int, my_type>::type,
        my_type
    >::value;

    ASSERT_TRUE(result);
}

TEST(TypeConditionExpand, elides_false_function_overrides) {
    ASSERT_TRUE(success == fint());
}

TEST(TypeConditionExpand, instantiates_template_only_if_true) {
    const bool result = type::eq<
        type::condition::expand<true, my_type, float, my_holder, my_holder>::type,
        my_type::my_personal_typedef
    >::value;

    ASSERT_TRUE(result);
}

TEST(TestConditionExpand, instantiates_template_only_if_false) {
    const bool result = type::eq<
        type::condition::expand<false, float, my_type, my_holder, my_holder>::type,
        my_type::my_personal_typedef
    >::value;

    ASSERT_TRUE(result);
}

