#include <gtest/gtest.h>
#include "cpp/integer.h"

TEST(CppPrev, prev_decrements_natruals_from_0_to_511) {

    ASSERT_EQ(999, PREV(0));
    ASSERT_EQ(0, PREV(1));
    ASSERT_EQ(9, PREV(10));
    ASSERT_EQ(10, PREV(11));
    ASSERT_EQ(99, PREV(100));
    ASSERT_EQ(100, PREV(101));
    ASSERT_EQ(199, PREV(200));
    ASSERT_EQ(200, PREV(201));
    ASSERT_EQ(299, PREV(300));
    ASSERT_EQ(300, PREV(301));
    ASSERT_EQ(399, PREV(400));
    ASSERT_EQ(400, PREV(401));
    ASSERT_EQ(499, PREV(500));
    ASSERT_EQ(500, PREV(501));
    ASSERT_EQ(511, PREV(512));
}

TEST(CppPrev, prev_is_transitive) {
    ASSERT_EQ(0, PREV(PREV(PREV(PREV(4)))));
    ASSERT_EQ(504, PREV(PREV(PREV(PREV(PREV(PREV(PREV(PREV(512)))))))));
}

TEST(CppPrev, prev_yields_single_token) {
    ASSERT_STREQ("0", STRINGIZE(PREV(PREV(PREV(PREV(4))))));
}

