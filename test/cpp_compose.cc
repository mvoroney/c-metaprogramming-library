
#include <gtest/gtest.h>
#include "cpp/list/fold.h"
#include "cpp/compose.h"
#include "cpp/apply.h"
#include "cpp/arithmetic.h"


TEST(CppLambda, left_function_composition_works) {

    ASSERT_EQ(0, LEFT_COMPOSE((0)));
    ASSERT_EQ(5, LEFT_COMPOSE((0, 2, 3), (ADD, (_2, _3))));
    ASSERT_EQ(2, LEFT_COMPOSE((0, 3, 6), (ADD, (_2, _3)), (SUB, (_1, 7))));
    ASSERT_EQ(5, LEFT_COMPOSE((0, 3, 6), (ADD, (_2, _3)), (SUB, (_1, 7)), (ADD, (_2, _1))));
}

TEST(CppLambda, right_function_composition_works) {

    //LIST_REVERSEL(0, COMMA, (0));
    //LIST_REVERSEL(0, COMMA, ((ADD, (2, _3)), (0, 1, 2)));
    ASSERT_EQ(0, RIGHT_COMPOSE((0)));
    ASSERT_EQ(5, RIGHT_COMPOSE((ADD, (_2, _3)), (0, 2, 3)));
    ASSERT_EQ(2, RIGHT_COMPOSE((SUB, (_1, 7)), (ADD, (_2, _3)), (0, 3, 6)));
    ASSERT_EQ(5, RIGHT_COMPOSE((ADD, (_2, _1)), (SUB, (_1, 7)), (ADD, (_2, _3)), (0, 3, 6)));
}



