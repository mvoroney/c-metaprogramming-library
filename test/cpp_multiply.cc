
#include <gtest/gtest.h>
#include "cpp/arithmetic/multiplier.h"
#include "cpp/util.h"


TEST(CppMul, mul_digits_2_works) {
    ASSERT_STREQ("0,0,0,0,0", STRINGIZE(MUL_DIGITS2_0(0, 0,0, 0,0)));
    ASSERT_STREQ("0,0,0,0,1", STRINGIZE(MUL_DIGITS2_0(0, 0,1, 0,1)));
    ASSERT_STREQ("0,0,0,0,2", STRINGIZE(MUL_DIGITS2_0(1, 0,1, 0,1)));
    ASSERT_STREQ("0,0,0,1,1", STRINGIZE(MUL_DIGITS2_0(0, 0,1, 1,1)));
    ASSERT_STREQ("0,0,1,2,1", STRINGIZE(MUL_DIGITS2_0(0, 1,1, 1,1)));
    ASSERT_STREQ("0,0,8,9,1", STRINGIZE(MUL_DIGITS2_0(0, 0,9, 9,9)));
    ASSERT_STREQ("0,0,8,9,1", STRINGIZE(MUL_DIGITS2_0(0, 9,9, 0,9)));
    ASSERT_STREQ("0,9,8,0,1", STRINGIZE(MUL_DIGITS2_0(0, 9,9, 9,9)));
}

TEST(CppMul, mul_digits_3_works) {
    ASSERT_STREQ("0,0,0,0,0,0,0", STRINGIZE(MUL_DIGITS3_0(0, 0,0,0, 0,0,0)));
    ASSERT_STREQ("0,0,0,0,0,0,1", STRINGIZE(MUL_DIGITS3_0(0, 0,0,1, 0,0,1)));
    ASSERT_STREQ("0,0,0,0,0,0,2", STRINGIZE(MUL_DIGITS3_0(1, 0,0,1, 0,0,1)));
    ASSERT_STREQ("0,0,0,0,1,1,1", STRINGIZE(MUL_DIGITS3_0(0, 0,0,1, 1,1,1)));
    ASSERT_STREQ("0,0,1,2,3,2,1", STRINGIZE(MUL_DIGITS3_0(0, 1,1,1, 1,1,1)));
    ASSERT_STREQ("0,9,9,8,0,0,1", STRINGIZE(MUL_DIGITS3_0(0, 9,9,9, 9,9,9)));
}

TEST(CppMul, mult_by_0_is_0) {
    ASSERT_EQ(0, MULT(0, 1));
    ASSERT_EQ(0, MULT(1, 0));
    ASSERT_EQ(0, MULT(0, 0));
    ASSERT_EQ(0, MULT(0, 4));
    ASSERT_EQ(0, MULT(6, 0));
}

TEST(CppMul, multiply_by_1_preserves_identity) {
    ASSERT_EQ(1, MULT(1, 1));
    ASSERT_EQ(9, MULT(1, 9));
    ASSERT_EQ(9, MULT(9, 1));
    ASSERT_EQ(4, MULT(4, 1));
    ASSERT_EQ(5, MULT(1, 5));
}

TEST(CppMul, multiply_without_overflow_works) {
    ASSERT_EQ(468, MULT(234, 2));
    ASSERT_EQ(891, MULT(99, 9));
    ASSERT_EQ(891, MULT(9, 99));
}

TEST(CppMul, multiply_with_overflow_works) {
    ASSERT_EQ(9801, MULT(99, 99));
}

TEST(CppMul, multiply_with_negative_numbers_works) {
    ASSERT_EQ(-20, INTEGER_SIGN(MULT(10, INTEGER_NEGATE(2))));
    ASSERT_EQ(-99, INTEGER_SIGN(MULT(INTEGER_NEGATE(1), 99)));
    ASSERT_EQ(-9, INTEGER_SIGN(MULT(INTEGER_NEGATE(1), 9)));
    ASSERT_EQ(-498, INTEGER_SIGN(MULT(3, INTEGER_NEGATE(166))));
    ASSERT_EQ(300, INTEGER_SIGN(MULT(INTEGER_NEGATE(20), INTEGER_NEGATE(15)))); 
}

