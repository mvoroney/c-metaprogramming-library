
#include <gtest/gtest.h>
#include "cpp/arithmetic.h"

TEST(CppSubtract, subtraction_works) {
    ASSERT_EQ(1, (SUB(3, 2)));
    ASSERT_EQ(0, (SUB(0, 0)));
    ASSERT_EQ(5, (SUB(5, 0)));
    ASSERT_EQ(38, (SUB(50, 12)));
    ASSERT_EQ(256, (SUB(384, 128)));
    ASSERT_EQ(511, (SUB(512, 1)));
    ASSERT_EQ(256, (SUB(512, 256)));
    ASSERT_EQ(0, (SUB(512, 512)));
}
