
#include <gtest/gtest.h>
#include "cpp/arithmetic/digits.h"
#include "cpp/arithmetic/adder.h"

TEST(CppInteger, digits_integer_works) {
    ASSERT_EQ(0, DIGITS_INTEGER(0, 0, 0, 0, 0));
    ASSERT_EQ(123, DIGITS_INTEGER(0, 0, 1, 2, 3));
    /// @todo is this the correct behavior?
    ASSERT_EQ(021, DIGITS_INTEGER(9, 9, 0, 2, 1));
    ASSERT_EQ(996, DIGITS_INTEGER(9, 9, 9, 9, 9, 9, 9, 9, 6));
    ASSERT_EQ(801, DIGITS_INTEGER(9, 8, 0, 1));
    ASSERT_EQ(9801, DIGITS_INTEGER(0, 9, 8, 0, 1));

    ASSERT_EQ(1, DIGITS_INTEGER(0, 0, 1));
    ASSERT_EQ(2, DIGITS_INTEGER(0, 2));
    ASSERT_EQ(3, DIGITS_INTEGER(3));
}
