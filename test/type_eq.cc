
#include <gtest/gtest.h>
#include "type/util.h"

template<typename T, typename U>
struct pair {
    typedef T first;
    typedef U second;
};

struct ClassType {};
union UnionType {};

typedef ::testing::Types<
    pair<char, unsigned char>,
    pair<short int, short int *>,
    pair<int, int &>,
    pair<long, const long>,
    pair<double, volatile double>,
    pair<float, int>,
    pair<ClassType, UnionType>,
    pair<const ClassType, const UnionType>
> test_types;

template<typename T>
class TypeEQ : public ::testing::Test {};

TYPED_TEST_CASE(TypeEQ, test_types);

TYPED_TEST(TypeEQ, type_eq_accepts_equal) {
    const bool first_result = type::eq<
        typename TypeParam::first,
        typename TypeParam::first
    >::value;
    const bool second_result = type::eq<
        typename TypeParam::second,
        typename TypeParam::second
    >::value;

    ASSERT_TRUE(first_result);
    ASSERT_TRUE(second_result);
}

TYPED_TEST(TypeEQ, type_eq_rejects_different) {
    const bool result = type::eq<
        typename TypeParam::first,
        typename TypeParam::second
    >::value;

    ASSERT_FALSE(result);
}

