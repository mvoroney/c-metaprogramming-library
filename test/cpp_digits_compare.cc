
#include <gtest/gtest.h>
#include "cpp/arithmetic/digits/compare.h"

TEST(CppDigits, equality_comparison_works) {
    ASSERT_EQ(1, DIGIT_EQ(0, 0));
    ASSERT_EQ(1, DIGIT_EQ(1, 1));
    ASSERT_EQ(1, DIGIT_EQ(2, 2));
    ASSERT_EQ(1, DIGIT_EQ(3, 3));
    ASSERT_EQ(1, DIGIT_EQ(4, 4));
    ASSERT_EQ(1, DIGIT_EQ(5, 5));
    ASSERT_EQ(1, DIGIT_EQ(6, 6));
    ASSERT_EQ(1, DIGIT_EQ(7, 7));
    ASSERT_EQ(1, DIGIT_EQ(8, 8));
    ASSERT_EQ(1, DIGIT_EQ(9, 9));

    ASSERT_EQ(0, DIGIT_EQ(0, 1));
    ASSERT_EQ(0, DIGIT_EQ(3, 2));
    ASSERT_EQ(0, DIGIT_EQ(8, 0));
}

TEST(CppDigits, less_than_comparison_works) {
    ASSERT_EQ(1, DIGIT_LT(0, 1));
    ASSERT_EQ(1, DIGIT_LT(0, 9));
    ASSERT_EQ(0, DIGIT_LT(2, 1));
    ASSERT_EQ(0, DIGIT_LT(5, 5));
}

TEST(CppDigits, greater_than_comparison_works) {
    ASSERT_EQ(1, DIGIT_GT(3, 2));
    ASSERT_EQ(1, DIGIT_GT(7, 4));
    ASSERT_EQ(0, DIGIT_GT(2, 7));
    ASSERT_EQ(0, DIGIT_GT(6, 6));
}

TEST(CppDigits, less_equal_comparison_works) {
    ASSERT_EQ(1, DIGIT_LE(4, 4));
    ASSERT_EQ(1, DIGIT_LE(2, 4));
    ASSERT_EQ(1, DIGIT_LE(5, 7));
    ASSERT_EQ(0, DIGIT_LE(6, 2));
}

TEST(CppDigits, greater_equal_comparison_works) {
    ASSERT_EQ(1, DIGIT_GE(6, 6));
    ASSERT_EQ(1, DIGIT_GE(5, 4));
    ASSERT_EQ(1, DIGIT_GE(8, 4));
    ASSERT_EQ(0, DIGIT_GE(3, 9));
}

