
#include <gtest/gtest.h>
#include "cpp/placeholder.h"
#include "cpp/arithmetic.h"

TEST(CppPlaceholder, eval_selects_placeholder_index) {
    ASSERT_EQ(1, PLACEHOLDER_EVAL(_1, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(2, PLACEHOLDER_EVAL(_2, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(3, PLACEHOLDER_EVAL(_3, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(4, PLACEHOLDER_EVAL(_4, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(5, PLACEHOLDER_EVAL(_5, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(6, PLACEHOLDER_EVAL(_6, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(7, PLACEHOLDER_EVAL(_7, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(8, PLACEHOLDER_EVAL(_8, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
    ASSERT_EQ(9, PLACEHOLDER_EVAL(_9, (1, 2, 3, 4, 5, 6, 7, 8, 9)));
}

TEST(CppPlaceholder, eval_passes_through_non_placeholder) {
    ASSERT_EQ(548, PLACEHOLDER_EVAL(548, (a, b, c, d)));
    ASSERT_STREQ("_548", STRINGIZE(PLACEHOLDER_EVAL(_548, (a, b, c, d))));
}

TEST(CppPlaceholder, evaln_substitutes_placeholders) {
    ASSERT_STREQ(
        "a, b, c, d, e",
        STRINGIZE(PLACEHOLDER_EVALN(5, (b, d, a, e, c), (_3, _1, _5, _2, _4))));
    ASSERT_STREQ(
        "a, b, c, d, e", 
        STRINGIZE(PLACEHOLDER_EVALN(5, (b, 435, d), (a, _1, c, _3, e))));
    ASSERT_STREQ(
        "a, b, c, d, e, g, f, j, k", 
        STRINGIZE(PLACEHOLDER_EVALN(9, (b, 435, d, f, g, h, i, j, k), (a, _1, c, _3, e, _5, _4, _8, _9))));
}

#define FOOBAR() 42
#define FOOBAZ(a, b, c, d, e) e
#define FOOBIZ(a, b, c, d, e, f, g, h, i) i

TEST(CppPlaceholder, evalm_works_for_0_to_9_arg_macros) {
    ASSERT_EQ(7, PLACEHOLDER_EVALM(ADD, 2, (3, 4), (_1, _2)));
    ASSERT_EQ(42, PLACEHOLDER_EVALM(FOOBAR, 0, (), ()));
    ASSERT_EQ(24, PLACEHOLDER_EVALM(FOOBAZ, 5, (24), (a, b, c, d, _1)));
    ASSERT_EQ(36, PLACEHOLDER_EVALM(FOOBIZ, 9, (36), (a, b, c, d, e, f, g, h, _1)));
}

TEST(CppPlaceholder, eval_accepts_parenthetical_arguments) {
    ASSERT_STREQ(
        "(a)",
        STRINGIZE(PLACEHOLDER_EVAL(_1, ((a), (b)))));
    ASSERT_STREQ(
        "(a)", 
        STRINGIZE(PLACEHOLDER_EVALM(FOOBAZ, 5, ((a), (b), (c)), (a, b, c, d, _1))));

}

