
#include <gtest/gtest.h>
#include "cpp/list.h"

#define LIST128 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128

TEST(CppList, front_accepts_empty_list) {
    ASSERT_STREQ("", STRINGIZE(LIST_FRONT(0,)));
    ASSERT_STREQ("", STRINGIZE(LIST_FRONT(0, ())));
    ASSERT_STREQ("0", STRINGIZE(LIST_FRONT(1, 0)));
}

TEST(CppList, back_accepts_empty_list) {
    ASSERT_STREQ("", STRINGIZE(LIST_BACK(0,)));
    ASSERT_STREQ("", STRINGIZE(LIST_BACK(0, ())));
    /// @todo problem case: when we ask for the remainder of a list after the last element.
    ASSERT_STREQ("", STRINGIZE(LIST_BACK(1, 0)));
}

TEST(CppList, front_works_to_128) {
    ASSERT_STREQ(
        "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13",
        STRINGIZE(LIST_FRONT(13, LIST128)));

    ASSERT_STREQ(
        "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67",
        STRINGIZE(LIST_FRONT(67, LIST128)));

    ASSERT_STREQ(
        "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97",
        STRINGIZE(LIST_FRONT(97, LIST128)));

    ASSERT_STREQ(
        STRINGIZE(LIST128),
        STRINGIZE(LIST_FRONT(128, LIST128,)));
}

TEST(CppList, back_works_to_128) {
    ASSERT_STREQ(
        "17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128",
        STRINGIZE(LIST_BACK(16, LIST128)));

    ASSERT_STREQ(
        "18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128",
        STRINGIZE(LIST_BACK(17, LIST128)));


    ASSERT_STREQ(
        "102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128",
        STRINGIZE(LIST_BACK(101, LIST128)));

    ASSERT_STREQ(
        "128",
        STRINGIZE(LIST_BACK(127, LIST128)));

    ASSERT_STREQ(
        "",
        STRINGIZE(LIST_BACK(128, LIST128, )));
}

TEST(CppList, select_works_to_128) {
    ASSERT_EQ(1, LIST_SELECT(0, LIST128));
    ASSERT_EQ(31, LIST_SELECT(30, LIST128));
    ASSERT_EQ(66, LIST_SELECT(65, LIST128));
    ASSERT_EQ(128, LIST_SELECT(127, LIST128));
}

TEST(CppList, insert_works_to_128) {
    
    ASSERT_STREQ(
        "1",
        STRINGIZE(LIST_INSERT(0, 1,)));

    ASSERT_STREQ(
        "1, 2",
        STRINGIZE(LIST_INSERT(0, 1, 2)));

    ASSERT_STREQ(
        "1, 2",
        STRINGIZE(LIST_INSERT(1, 2, 1)));

    ASSERT_STREQ(
        "1, 2, 3",
        STRINGIZE(LIST_INSERT(2, 3, 1, 2)));

    ASSERT_STREQ(
        "1, 2, 3, 4, 5, 6, 7, 8, 9, 10",
        STRINGIZE(LIST_INSERT(5, 6, 1, 2, 3, 4, 5, 7, 8, 9, 10)));
}

TEST(CppList, join_works_for_any_lists) {
    ASSERT_STREQ(
        "",
        STRINGIZE(LIST_JOIN(,)));
    ASSERT_STREQ(
        "",
        STRINGIZE(LIST_JOIN((), ())));
    ASSERT_STREQ(
        "1",
        STRINGIZE(LIST_JOIN(, 1)));
    ASSERT_STREQ(
        "1",
        STRINGIZE(LIST_JOIN((), (1))));
    ASSERT_STREQ(
        "1",
        STRINGIZE(LIST_JOIN((1), )));
    ASSERT_STREQ(
        "1, 2",
        STRINGIZE(LIST_JOIN(1, (2))));
    ASSERT_STREQ(
        "1, 2",
        STRINGIZE(LIST_JOIN((1), 2)));
    ASSERT_STREQ(
        "1, 2, 3, 4, 5",
        STRINGIZE(LIST_JOIN((1, 2), (3, 4, 5))));
}

#define VA_ARGS_LENGTH(...) LIST_LENGTH(__VA_ARGS__)

TEST(CppList, length_works_to_128) {
    // NB: issues a warning regarding non-standard compliant use of variadic
    // macro arguments. requires at least 1 arg.
    ASSERT_EQ(0, LIST_LENGTH());
    ASSERT_EQ(0, VA_ARGS_LENGTH());
    ASSERT_EQ(1, LIST_LENGTH(()));

    ASSERT_EQ(1, LIST_LENGTH(a0));
    ASSERT_EQ(5, LIST_LENGTH(a0, a1, a2, a3, a4));
    ASSERT_EQ(33, LIST_LENGTH(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32));

    ASSERT_EQ(69, LIST_LENGTH(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59, a60, a61, a62, a63, a64, a65, a66, a67, a68));
    
    ASSERT_EQ(111, LIST_LENGTH(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59, a60, a61, a62, a63, a64, a65, a66, a67, a68, a69, a70, a71, a72, a73, a74, a75, a76, a77, a78, a79, a80, a81, a82, a83, a84, a85, a86, a87, a88, a89, a90, a91, a92, a93, a94, a95, a96, a97, a98, a99, a100, a101, a102, a103, a104, a105, a106, a107, a108, a109, a110));

    ASSERT_EQ(128, LIST_LENGTH(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59, a60, a61, a62, a63, a64, a65, a66, a67, a68, a69, a70, a71, a72, a73, a74, a75, a76, a77, a78, a79, a80, a81, a82, a83, a84, a85, a86, a87, a88, a89, a90, a91, a92, a93, a94, a95, a96, a97, a98, a99, a100, a101, a102, a103, a104, a105, a106, a107, a108, a109, a110, a111, a112, a113, a114, a115, a116, a117, a118, a119, a120, a121, a122, a123, a124, a125, a126, a127));
}

//LIST_INSERT(3, barbidol, a, b, c, d, e, f, g)
