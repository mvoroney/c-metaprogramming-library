
#include <gtest/gtest.h>
#include "type/util.h"
#include "type/traits.h"

struct my_other_type {};

struct my_type {
    typedef my_other_type value_type;
};

TEST(TypeTraitsValueType, reports_with_value_type) {
    const bool result = type::eq<
        type::traits<my_type>::value_type,
        my_type::value_type
    >::value;

    ASSERT_TRUE(result);
}

TEST(TypeTraitsValueType, pass_through_without_value_type) {
    const bool result = type::eq<
        type::traits<int>::value_type,
        int
    >::value;

    ASSERT_TRUE(result);
}
