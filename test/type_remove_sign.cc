
#include <gtest/gtest.h>
#include "type/util.h"
#include "type/manip.h"

template<typename T, typename U>
struct pair {
    typedef T input;
    typedef U expected;
};

struct ClassType {};
union UnionType {};

typedef ::testing::Types<
    pair<signed char, unsigned char>,
    pair<signed short int, unsigned short int>,
    pair<signed int, unsigned int>,
    pair<signed long int, unsigned long int>,
    pair<signed long long int, unsigned long long int>,
    pair<char, unsigned char>,
    pair<short int, unsigned short int>,
    pair<int, unsigned int>,
    pair<long int, unsigned long int>,
    pair<long long int, unsigned long long int>,
    pair<unsigned char, unsigned char>,
    pair<unsigned short int, unsigned short int>,
    pair<unsigned int, unsigned int>,
    pair<unsigned long int, unsigned long int>,
    pair<unsigned long long int, unsigned long long int>,
    pair<float, float>,
    pair<double, double>,
    pair<long double, long double>,
    pair<ClassType, ClassType>,
    pair<UnionType, UnionType>
> test_types;

typedef ::testing::Types<
    pair<const char, const unsigned char>,
    pair<volatile char, volatile unsigned char>,
    pair<char *, unsigned char *>,
    pair<char &, unsigned char &>
> expected_failures;

template<typename T>
class TypeRemoveSign : public ::testing::Test {};

template<typename T>
class TypeRemoveSignExpectedFailures : public ::testing::Test {};

TYPED_TEST_CASE(TypeRemoveSign, test_types);
TYPED_TEST_CASE(TypeRemoveSignExpectedFailures, expected_failures);

TYPED_TEST(TypeRemoveSign, remove_sign_makes_type_unsigned) {
    const bool result = type::eq<
        typename type::remove::sign<typename TypeParam::input>::type, 
        typename TypeParam::expected
    >::value;

    ASSERT_TRUE(result);
}

TYPED_TEST(TypeRemoveSignExpectedFailures, remove_sign_makes_type_unsigned) {
    const bool result 
        = type::eq<
            typename type::remove::sign<typename TypeParam::input>::type, 
            typename TypeParam::expected
        >::value;

    EXPECT_FALSE(result);
}

