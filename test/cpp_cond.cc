
#include <gtest/gtest.h>
#include "cpp/logic.h"

TEST(CppLogic, first_works_for_one_or_more_elements) {
    ASSERT_EQ(1, FIRST(1));
    ASSERT_EQ(1, FIRST(1, 2, 3, 4));
}

TEST(CppLogic, second_works_for_two_or_more_elements) {
    ASSERT_EQ(1, SECOND(0, 1));
    ASSERT_EQ(2, SECOND(1, 2, 3, 4, 5, 6));
}

TEST(CppLogic, isparenthetical_detects_parens) {
    ASSERT_EQ(1, ISPARENTHETICAL(()));
    ASSERT_EQ(1, ISPARENTHETICAL((x + y)));
    ASSERT_EQ(0, ISPARENTHETICAL(1));
    ASSERT_EQ(0, ISPARENTHETICAL(0));
}

TEST(CppLogic, iszero_accepts_near_arbitrary_input) {
    ASSERT_EQ(1, ISZERO(0));
    ASSERT_EQ(0, ISZERO((x + y)));
    ASSERT_EQ(0, ISZERO(1));
    ASSERT_EQ(0, ISZERO(x));
}

TEST(CppLogic, isblank_accepts_near_arbitrary_input) {
    ASSERT_EQ(1, ISBLANK(BLANK()));
    ASSERT_EQ(0, ISBLANK(()));
    ASSERT_EQ(0, ISBLANK(x));
    ASSERT_EQ(0, ISBLANK(1));
}

TEST(CppLogic, isempty_accepts_near_arbitrary_input) {
    ASSERT_EQ(1, ISEMPTY(()));
    ASSERT_EQ(0, ISEMPTY((0)));
    ASSERT_EQ(0, ISEMPTY((1)));
}

TEST(CppLogic, or_works) {
    ASSERT_EQ(1, OR(1, 1));
    ASSERT_EQ(1, OR(1, 0));
    ASSERT_EQ(1, OR(0, 1));
    ASSERT_EQ(0, OR(0, 0));
}

TEST(CppLogic, or_does_short_circuit_eval) {
    ASSERT_EQ(1, OR(0, ISTRUE, (9)));
    ASSERT_EQ(0, OR(0, ISTRUE, (0)));
    /// @note ISFOO does not exist.
    ASSERT_EQ(1, OR(1, ISFOO, ()));
}

TEST(CppLogic, and_works) {
    ASSERT_EQ(1, AND(1, 1));
    ASSERT_EQ(0, AND(1, 0));
    ASSERT_EQ(0, AND(0, 1));
    ASSERT_EQ(0, AND(0, 0));
}

TEST(CppLogic, and_does_short_circuit_eval) {
    ASSERT_EQ(1, AND(1, ISTRUE, (9)));
    ASSERT_EQ(0, AND(1, ISTRUE, (0)));
    /// @note ISFOO and ISBAR do not exist.
    ASSERT_EQ(0, AND(0, ISFOO, ()));
    ASSERT_EQ(0, AND(0, ISBAR, (~, ~)));
}

TEST(CppLogic, implies_works) {
    ASSERT_EQ(1, IMPLIES(1, 1));
    ASSERT_EQ(0, IMPLIES(1, 0));
    ASSERT_EQ(1, IMPLIES(0, 1));
    ASSERT_EQ(1, IMPLIES(0, 0));
}

TEST(CppLogic, implies_does_short_circuit_eval) {
    ASSERT_EQ(1, IMPLIES(1, ISTRUE, (9)));
    ASSERT_EQ(0, IMPLIES(1, ISTRUE, (0)));
    /// @note ISFOO and ISBAR do not exist.
    ASSERT_EQ(1, IMPLIES(0, ISFOO, ()));
    ASSERT_EQ(1, IMPLIES(0, ISBAR, (~, ~)));
}

TEST(CppLogic, not_works) {
    ASSERT_EQ(1, NOT(0));
    ASSERT_EQ(0, NOT(1));
}

TEST(CppLogic, istrue_discriminates_true_false) {
    ASSERT_EQ(1, ISTRUE(1));
    ASSERT_EQ(1, ISTRUE(foo));
    ASSERT_EQ(0, ISTRUE(BLANK()));
    ASSERT_EQ(0, ISTRUE(()));
    ASSERT_EQ(0, ISTRUE(0));
}

TEST(CppLogic, iif_selects_0_1) {
    ASSERT_EQ(42, IIF(0)(24, 42));
    ASSERT_EQ(42, IIF(1)(42, 24));
}

