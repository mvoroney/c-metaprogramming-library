
#include <gtest/gtest.h>
#include "type/util.h"
#include "type/check.h"

struct my_other_type {};

struct my_type {
    typedef my_other_type my_special_typedef;
};

WITH_TYPEDEF(my_special_typedef);

TEST(TypeHasMemberType, detects_member_type) {
    const bool test_is_valid = ! type::eq<
        my_type, 
        my_type::my_special_typedef
    >::value;
    const bool result_value = type::has::member<
        my_type, 
        with_my_special_typedef
    >::value;

    ASSERT_TRUE(test_is_valid);
    ASSERT_TRUE(result_value);
}

TEST(TypeHasMemberType, rejects_type_without_member_type) {
    const bool result = type::has::member<
        int,
        with_my_special_typedef
    >::value;

    ASSERT_FALSE(result);
}


