
#include <gtest/gtest.h>
#include "type/placeholder.h"
#include "type/util.h"


#define TYPES char, short, unsigned int, int, double

TEST(TypeLambdaPlaceholder, extracts_named_arg) {
    using namespace type::lambda::placeholder;

    ASSERT_TRUE((type::eq<_1::apply<TYPES>::type, char>::value));
    ASSERT_TRUE((type::eq<_2::apply<TYPES>::type, short>::value));
    ASSERT_TRUE((type::eq<_3::apply<TYPES>::type, unsigned int>::value));
    ASSERT_TRUE((type::eq<_4::apply<TYPES>::type, int>::value));
    ASSERT_TRUE((type::eq<_5::apply<TYPES>::type, double>::value));
}


