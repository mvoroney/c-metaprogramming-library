
#include <gtest/gtest.h>
#include "cpp/list/filter.h"
#include "cpp/logic.h"

#define NZ_COND(a) NOT(ISZERO(a))

TEST(CppList, filter_removes_selected_elements) {
    ASSERT_STREQ("", STRINGIZE(LIST_FILTER(NZ_COND, (_1), COMMA,)));
    ASSERT_STREQ("", STRINGIZE(LIST_FILTER(NZ_COND, (_1), COMMA, 0)));
    ASSERT_STREQ("1", STRINGIZE(LIST_FILTER(NZ_COND, (_1), COMMA, 0, 1, 0)));
    ASSERT_STREQ(
        "1,2,3,4,5,6,7,8,9,10,11,100",
        STRINGIZE(LIST_FILTER(NZ_COND, (_1), COMMA, 0, 1, 0, 2, 0, 0, 3, 0, 4, 5, 6, 0, 7, 0, 8, 0, 9, 0, 10, 11, 100, 0, 0, 0)));
}

