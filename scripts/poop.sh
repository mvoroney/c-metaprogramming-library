#!/bin/bash
set -u

POOP_CONFIG=""

## @cond
HELP_TEXT=$(cat <<'HELP_TEXT_CONTENTS' | sed -e 's/^## \?//' -e 's/@par //' -e 's/@param /       /'
## @endcond
## @file poop.sh
## 
## @par SYNOPSIS
## 
##     poop.sh [[-n <num_elements>] [-i <increment>] [-d <dir-template-extension>] 
##              [-f <file-template-extension>] [-o <output-extension>]
##             | -c <config-file> | --no-config | -h] <output>=<template> ...
##
## @par DESCRIPTION
##
## This script is the driver for a templating system based on bash variable
## substitution. A template is simply a file that contains zero or more bash
## variable dereferences. These will be interpolated and expanded in place.
## Any characters which must be escaped under bash string interpolation must
## also be escaped in a template file. Examples include ` ", $, \, ``` `.
## These can all be escaped with a \ character.
## 
## There are two types of templates. File templates and directory templates.
## A file template maps to a single output file, and a directory template maps
## to one or more files in a directory. With directory templates the total
## number of elements generated will be distributed across `ciel(num / inc)` 
## files.
##
## @par OPTIONS
##
## @param -n|\--num               The total number of elements to generate,
##                                possibly distributed across one or more
##                                output files.
## @param -i|\--inc               The maximum number of elements to include in
##                                any given output file.
## @param -d|\--dir-template-ext  The extension directory template files are
##                                expected to have. eg. ".d.h"
## @param -f|\--file-template-ext The extension file template files are
##                                expected to have. eg. ".f.h"
## @param -o|\--output-ext        The extension to give the output files.
##                                eg. ".h"
## @param -c|\--config            The path to a config file specifying the
##                                options to be used in this script. Additional
##                                variables can be specified here to extend the
##                                default set of variables available in
##                                template files.           
## @param \--no-config            Suppresss warnings related to missing
##                                configuration file.
## @param -h|\--help              Display this help message
##
## @par AUTHOR 
##
## Matt Voroney
##
## @cond
HELP_TEXT_CONTENTS
)
## @endcond

## @fn generate_file
##
## generates an output file with variable substitutions. The following variables
## will be available for use within the template file:
##
## - `$SEQ`     The sequence number of the file being generated.
## - `$START`   The element number to start at.
## - `$END`     The element number to end at.
## - `$ARGLIST` A comma separated list of arguments from `a0` to `a<$START>`
##              This is useful for generating macros that forward long sublists of
##              arguments.
##
## @param template The path of the template file to interpret.
## @param seq      The sequence number of the file being generated. Where
##                 multiple files are generated from a single template, this
##                 number represents which pass is being performed. This
##                 parameter will be available in the template as `$SEQ`
## @param START    The element number to start at. This parameter will be
##                 available in the template as `$START`.
## @param END      The element number to end at. This parameter will be
##                 available in the template as `$END`.
##
## @note Extra variables can be made available to the template through the config file.
##
function generate_file() {
    local file="${1:-'/dev/stdout'}"
    local template="${2:-}"
    local SEQ=${3:-0}
    local START=${4:-0}
    local END=${5:-0}

    local ARGLIST=`seq -s ', ' 0 $START | sed 's/[0-9][0-9]*/a&/g'`
 
    # ensure parent directory exists.
    mkdir -p "$(dirname "$file")"
    eval "echo \"$(cat $template)\" > \"$file\""
}

## @fn generate_directory
##
## Generates a directory and a set of files from a template, where each file
## handles a partition of the elements.
##
## @param directory the path of the directory to generate
## @param template  the name of the template to generate the contents of the
##                  directory from.
## @param name_rule The name of a function to use to generate to generate
##                  filenames for the directory. The function will be passed the
##                  output directory path, and the sequence number. i.e.
##                  `$name_rule $directory $SEQ` will be used to generate the
##                  file name. If extra directory elements are added, they will
##                  be created by this function as part of the generation process.
## @param end       The element number to end at. If not set, the default value
##                  is 0.
## @param inc       The maximum number of elements to place in an output file.
##                  If not set, the default value is 127.
##
function generate_directory() {
    local directory="${1:-}"
    local template="${2:-}"
    local name_rule="${3:-}"
    local end=$((${4:-0} - 1))
    
    local inc=${5:-127}
    local seq=0

    local j=
    local ofname=

    rm -rf "$directory"
    mkdir -p "$directory"

    # @todo should $end be decremented again here? or is this a mistake?
    
    for i in `seq 0 $inc $((end - 1))`; do
        j=$(($i + $inc))
        if [ $j -gt $end ]; then j=$end; fi
        ofname="$($name_rule "$directory" $seq)"
        generate_file "$ofname" "$template" $seq $i $j
        seq=$(($seq + 1))
    done
}

## @fn generate_component
##
## Generates an output component from an input component. The input component
## may be a file or a directory, which specifies the generation of a similar
## file or directory structure (the output component).
## 
## @param pathroot     The path to use as the root of the generated output
##                     component.
## @param templateroot The path of the input component. This is expected to be
##                     the path to a template file or directory, without the
##                     extension. `${templateroot}${ft_ext}`,
##                     `${templateroot}${dt_ext}` will be tested to see if a
##                     file with that path exists. Failing that,
##                     `${templateroot}` will be tested to see if a directory
##                     with that path exists.
## @param name_rule    The name of a function to be used to generate output
##                     file names with.
## @param num_elements The number of elements to generate in the output fileset.
## @param inc          The number of elements to include in each generated file.
## @param ft_ext       The file template file extension.
## @param dt_ext       The directory template file extension.
##
## @see _default_name_rule      for an example implementation of a name_rule
##                              function.
## @see generate_file_component for details on the requirements of the name_rule
##                              function.
##
function generate_component() {
    local pathroot="${1:-}"
    local templateroot="${2:-}"
    local name_rule="${3:-}"
    local n_elements=${4:-0}
    local inc=${5:-0}
    local ftext="${6:-}"
    local dtext="${7:-}"
    
    # @todo is this the correct behavior?
    if [ -f "${templateroot}" ]; then
        ftext=""
        dtext=""
    fi
    
    if [ -f "${templateroot}${ftext}" -o -f "${templateroot}${dtext}" ]; then
    
        generate_file_component "$pathroot" "$templateroot" "$ftext" "$dtext" $name_rule $n_elements $inc

    elif [ -d "${templateroot}" ]; then
       
        generate_dir_component "$pathroot" "$templateroot" "$ftext" "$dtext" $name_rule $n_elements $inc
    
    fi
}

## @fn generate_dir_component
##
## Generates a component from an input directory, which may contain 0 or more
## templates, and places the output in the output directory, with a similar
## directory structure to the input directory.
##
## @param pathroot     The path to use as the output root.
## @param templateroot The path of the input component.
## @param ft_ext       The file template file extension.
## @param dt_ext       The directory template file extension.
## @param name_rule    The name of a function to be used to generate output
##                     file names with.
## @param num_elements The number of elements to generate in the output fileset.
## @param inc          The number of elements to include in each generated file.
##
## @see generate_file_component for detailed information on each of the parameters
##                              outlined here. This function delegates to 
##                              generate_file_component().
##
function generate_dir_component() {
    local pathroot="${1:-}"
    local templateroot="${2:-}"
    local ftext="${3:-}"
    local dtext="${4:-}"
    local name_rule="${5:-}"
    local n_elements=${6:-0}
    local inc=${7:-0}

    local templates=`find "${templateroot}" -name "*${ftext}" -o -name "*${dtext}" \
        | sed "s=^${templateroot}\(.*\)\($ftext\|$dtext\)$=\1=" \
        | uniq`

    for t in $templates; do
        
        generate_file_component "${pathroot}/$t" "${templateroot}/$t" "$ftext" "$dtext" $name_rule $n_elements $inc
        
    done
}

## @fn generate_file_component
##
## Generates a component from an input file and/or directory template.
##
## @param path         The path of the output file to generate, without the
##                     file extension.
## @param template     The path of the template file to use, without the file
##                     extension.
## @param ft_ext       The file template file extension.
## @param dt_ext       The directory template file extension.
## @param name_rule    The name of a function to be used to generate output
##                     file names with.
## @param num_elements The number of elements to generate in the output file or
##                     fileset. Defaults to 0 if not set.
## @param inc          The number of elements to include in each generated file
##
## @see generate_file
## @see generate_directory
##
function generate_file_component() {
    local path="${1:-}"
    local template="${2:-}"
    local ftext="${3:-}"
    local dtext="${4:-}"
    local name_rule="${5:-}"
    local n_elements=${6:-0}
    local inc=${7:-0}

    if [ -f "${template}${ftext}" ]; then
        generate_file "$($name_rule "$path")" "${template}${ftext}" 0 0 $(($n_elements - 1))
    fi
    if [ "$ftext" != "$dtext" -a -f "${template}${dtext}" ]; then
        generate_directory "$path" "${template}${dtext}" $name_rule $n_elements $inc
    fi
}

## @fn _default_name_rule
##
## Generates an output file name based on supplied directory/file components. If
## the second parameter is not supplied or empty, the first parameter is assumed
## to be a complete path, and the $OUTPUT_EXTENSION will simply be apended to
## this. The generated path is written to standard output.
##
## @param path The path of the output file or directory to generate. if `file` is
##             provided, `path` is assumed to be a directory. Otherwise `path` is
##             assumed to be the full path of the output file without the file
##             extension. The file extension is taken from `$OUTPUT_EXTENSION`.
## @param file The name of the file component of the path, without the file
##             extension. The extension is taken from `$OUTPUT_EXTENSION`
##
function _default_name_rule() {
    if [ -z "${2:-}" ]; then
        echo "${1:-}${OUTPUT_EXTENSION}" #file name
    else
        echo "${1:-}/${2:-}${OUTPUT_EXTENSION}" #dir element name
    fi
}

## @fn opt_check
##
## Checks to make sure that the supplied argument is part of a well formed
## option - value pair, of the form `-o <value>` or `\--option <value>`.
##
## @param value     The value of the option being checked.
## @param option    The name of the option being checked.
## @param arg_count The number of arguments remaining.
##
## @return 0 if well formed.
## @return 1 if not well formed. An error message will be written to STDERR.
##
function opt_check() {
    if [ "-" != "${1:0:1}" -a ${3:-0} -gt 0 ]; then
        return 0
    else
        echo "missing argument to '--${2:-}': ignored" >&2
        return 1
    fi
}

## @fn configure
##
## Sets up the default configuration values and reads in the config file
## if found. Config file values will override default configuration values.
##
function configure() {
    # default config values
    NUM_ELEMENTS=1000
    INCREMENT=100
    DIR_TEMPLATE_EXTENSION=""
    NAME_RULE="_default_name_rule"
    FILE_TEMPLATE_EXTENSION=""
    OUTPUT_EXTENSION=""
    MAPPINGS=""
    CONFIGURED="no"
    NO_CONFIG=0
    POOP_MODS="$(dirname ${0:-})/poopmods"
    
    # pull in config file
    if [ -f "$POOP_CONFIG" ]; then
        . "$POOP_CONFIG"
        CONFIGURED="yes"
    elif [ -f "Poopfile" ]; then
        . "Poopfile"
        CONFIGURED="yes"
    fi
}


## @fn parse_opts
##
## parses the supplied argument list.
##
## @see poop.sh for a detailed description of the options recognized. 
##
function parse_opts() {
    while [ $# -gt 0 ]; do
        case $1 in
        -n|--num)
            if shift && opt_check "$1" "num" "$#"; then
                NUM_ELEMENTS=$1
                shift
            fi
            ;;
        -i|--inc)
            if shift && opt_check "$1" "inc" "$#"; then
                INCREMENT=$1
                shift
            fi
            ;;
        -d|--dir-template-ext)
            if shift && opt_check "$1" "dir-template-ext" "$#"; then
                DIR_TEMPLATE_EXTENSION=$1
                shift
            fi
            ;;
        -f|--file-template-ext)
            if shift && opt_check "$1" "file-template-ext" "$#"; then
                FILE_TEMPLATE_EXTENSION=$1
                shift
            fi
            ;;
        -o|--output-ext)
            if shift && opt_check "$1" "output-ext" "$#"; then
                OUTPUT_EXTENSION=$1
                shift
            fi
            ;;
        -c|--config)
            if shift && opt_check "$1" "config" "$#"; then
                POOP_CONFIG=$1
                shift
            fi
            ;;
        --no-config)
            NO_CONFIG=1
            shift
            ;;
        -h|--help)
            echo "$HELP_TEXT"
            exit 0; 
            ;;
        -*)
            echo "Unrecognized option $1" >&2
            shift
            ;;
        *=*)
            MAPPINGS+="$1"$'\n'
            shift
            ;;
        *)
            if [ $# -gt 1 -a "${2:0:1}" != "-" ]; then
                MAPPINGS+="$1=$2"$'\n'
                shift 2
            else
                echo "template spec missing for component '$1': cannot generate" >&2
                shift
            fi
            ;;
        esac
    done


    if [ $NO_CONFIG == "0" -a $CONFIGURED == "no" ]; then
        echo "Could not find config file! pass '-no-config' to skip this check. Otherwise, a"
        echo "config file must be found at the path indicated by the \$POOP_CONFIG"
        echo "enironment variable, or named 'Poopfile' in the current directory."
        exit 1
    fi

}

## @fn load_mods
##
## load plugin modules. The directory tree rooted at `$POOP_MODS` is searched
## for files with a `.pm` extension. These are expected to be shell scripts
## containing functions that can be used within templates. Each `.pm` is sourced
## directly by this script.
##
function load_mods() {
    if [ -d "$POOP_MODS" ]; then
        for i in `find "$POOP_MODS" -type f -name *.pm`; do
            . $i
        done
    fi
}

function generate_components() {

    configure
    parse_opts $@
    load_mods

    while IFS= read -r m && [ "$m" != "" ]; do
        PATHROOT="${m%%=*}"
        TEMPLATEROOT="${m##*=}"

        generate_component "$PATHROOT" "$TEMPLATEROOT" $NAME_RULE $NUM_ELEMENTS $INCREMENT $FILE_TEMPLATE_EXTENSION $DIR_TEMPLATE_EXTENSION 
    done <<<"$MAPPINGS"

    exit 0
}

generate_components $@

