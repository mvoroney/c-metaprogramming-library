
## @file
##
## @author Matt Voroney
##

##
## Builds a macro definition from a series of related macros
##
function serial_macro() {
    local name=$1
    local number=$2
    shift 2
    local increments=$(for i in $*; do echo $i; done | sort -r -n | uniq)

    local invocation="n"
    
    # build macro invocation
    local r=$number
    for i in $increments; do
        while [ $r -ge $i ]; do
            r=$(($r - $i))
            invocation="${name}${i}(${invocation})"
        done
    done

    echo "#define ${name}${number}(n) $invocation"
}

##
## Injects processed template output into a stream. Caller is responsible for
## ensuring that all parameters for the template are initialized.
##
function use_template() {
    local template="$1"
    local IFS=:
    local path=":$POOP_PATH"
    
    for p in $path; do
        if [ -f "${p}/${template}" ]; then
            IFS=" "
            eval "echo \"$(cat ${p}/${template})\""

            return 0
        fi
    done

    echo "template \"${template}\" does not exist on path: \"$path\"." >&2
    return 1 
}

