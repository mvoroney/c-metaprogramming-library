#!/bin/sh

preamble_file=$(tempfile)

cat > $preamble_file <<PREAMBLE_BLOCK
/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
PREAMBLE_BLOCK

preamble_bytes=$(wc -c $preamble_file | { read first rest; echo $first; })

for location in $@
do
    for file in `find $location -type f`
    do

        cmp -s -n$preamble_bytes $preamble_file $file

	if [ $? -ne 0 ]
        then
            temp_src_file=$(tempfile)

            cat $preamble_file $file > $temp_src_file
	    mv -f $temp_src_file $file
            echo added licence to $file
        fi
    done
done

#rm -f $preamble_file

