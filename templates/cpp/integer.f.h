/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated, please don't edit it */
/* temporary compatibility stub */
/**
 * @file
 *
 * This file contains primitives to allow simple integer sequences to be
 * manipulated. This is primarily useful for loop construction.
 *
 * @see cpp/arithmetic.h for a more complete numeric processor.
 *
 * @author Matt Voroney matt.voroney@gmail.com
 */
#pragma once
#ifndef CPP_INTEGER_H
#define CPP_INTEGER_H

#include \"cpp/integer/relative.h\"

/**
 * Finds the c'th previous integer of n.
 * @note only defined for 0 <= c <= 100
 */
#define PREVN(c, n) INTEGER_PREVN(c, n) 

/**
 * Finds the c'th next integer of n.
 * @note only defined for 0 <= c <= 100
 */
#define NEXTN(c, n) INTEGER_NEXTN(c, n)

/**
 * Retrieves the next integer in the sequence
 *
 * @param n - The integer to retrieve the successor of.
 * @return The successor of n.
 */
#define NEXT(n) INTEGER_NEXT(n) 

/**
 * Retrieves the previous integer in the sequence
 * 
 * @param n - The integer to retrieve the predecesor of.
 * @return The predecessor of n.
 */
#define PREV(n) INTEGER_PREV(n) 


#endif /* CPP_INTEGER_H */

