/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated, please don't edit it. */
#pragma once
#ifndef CPP_INTEGER_RELATIVE_H
#define CPP_INTEGER_RELATIVE_H

#include \"cpp/util.h\"

#define INTEGER_PREV(n) INTEGER_PREV1(n)
#define INTEGER_NEXT(n) INTEGER_NEXT1(n)

#define INTEGER_PREVN(c, n) PASTE(INTEGER_PREV, c)(n)
#define INTEGER_NEXTN(c, n) PASTE(INTEGER_NEXT, c)(n)

#define INTEGER_PREV_EXPAND(...) __VA_ARGS__
#define INTEGER_NEXT_EXPAND(...) __VA_ARGS__
#define INTEGER_PREV_(a, b) a
#define INTEGER_NEXT_(a, b) b

#include \"cpp/integer/relative/0.h\"
$(for i in 1 2 4 8 16 32 64 128 256 512; do
    echo "#include \"cpp/integer/relative${i}/0.h\""
done)

#ifdef INTEGER_MAX
$(for i in `seq 1 $(($NUM_ELEMENTS / $INCREMENT))`; do
    echo "#    if (INTEGER_MAX > $(($i * $INCREMENT)))"
    echo "#        include \"cpp/integer/relative/${i}.h\""
    for j in 1 2 4 8 16 32 64 128 256 512; do
        echo "#        include \"cpp/integer/relative${j}/${i}.h\""
    done
    echo "#    endif"
done)
#endif

#endif /* CPP_INTEGER_RELATIVE_H */

