/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated, please don't edit it. */
#pragma once

$(for i in `seq $START $END`; do
    if [ $i -lt 32 ]; then
        echo "#define INTEGER_RELATIVE32_${i} $(($NUM_ELEMENTS - $i - 32)),$(($i + 32))"
    elif [ $i -gt $(($NUM_ELEMENTS - 33)) ]; then
        echo "#define INTEGER_RELATIVE32_${i} $(($i - 32)),$(($i - $NUM_ELEMENTS + 32))"
    else
        echo "#define INTEGER_RELATIVE32_${i} $(($i - 32)),$(($i + 32))"
    fi
done)

