/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated, please don't edit it. */
#pragma once

$(for i in `seq $START $END`; do
    if [ $i -lt 512 ]; then
        echo "#define INTEGER_RELATIVE512_${i} $(($NUM_ELEMENTS + $i - 512)),$(($i + 512))"
    elif [ $i -gt $(($NUM_ELEMENTS - 513)) ]; then
        echo "#define INTEGER_RELATIVE512_${i} $(($i - 512)),$(($i - $NUM_ELEMENTS + 512))"
    else
        echo "#define INTEGER_RELATIVE512_${i} $(($i - 512)),$(($i + 512))"
    fi
done)

