/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated, please don't edit it. */
#pragma once
#ifndef CPP_INTEGER_DIGITS3_H
#define CPP_INTEGER_DIGITS3_H

#include \"cpp/util.h\"
#include \"cpp/integer/digits3/0.h\"

/**
 * Converts the supplied number to a 3 digit-list.
 * Supports 0 to $((NUM_ELEMENTS - 1)) in base-10
 */
#define INTEGER_DIGITS3(n) PASTE(INTEGER_DIGITS3_, n)

#ifdef DIGITS3_MAX
$(for i in `seq 1 $(($NUM_ELEMENTS / $INCREMENT))`; do
    echo "#    if (DIGITS3_MAX > $(($i * $INCREMENT)))"
    echo "#        include \"cpp/digits3/${i}.h\""
    echo "#    endif"
done)
#endif

#endif /* CPP_INTEGER_DIGITS3_H */

