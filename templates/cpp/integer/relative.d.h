/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated, please don't edit it. */
#pragma once

$(increments="512 256 128 64 32 16 8 4 2 1"

for macro in INTEGER_PREV INTEGER_NEXT; do
    for i in `seq $START $END`; do
        if [ `expr match "$increments" ".*\b$i\b"` -gt 0 ]; then
            echo "#define ${macro}$i(n) ${macro}_EXPAND(DEFER(1, ${macro}_)(PASTE(INTEGER_RELATIVE${i}_, n)))"
        else
            serial_macro $macro $i $increments
        fi
    done
    echo ""
done)

