/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated, please don't edit it. */
/**
 * @todo should this be done with HORIZONTAL_WHILE() or something similar instead?
 */
#pragma once
#ifndef CPP_LIST_GENERATE_H
#define CPP_LIST_GENERATE_H

#include \"cpp/util.h\"
#include \"cpp/list/generate/0.h\"

/**
 * Generates a comma separated list of numbers from 0 to n-1. The most common
 * use of this facility will be in conjunction with LIST_MAP to build iterative
 * facilities.

 * @see LIST_MAP
 */
#define LIST_GENERATE(n) PASTE(LIST_GENERATE, n)

#ifdef LIST_MAX
$(for i in `seq 1 $(($NUM_ELEMENTS / $INCREMENT))`; do
    echo "#    if (LIST_MAX > $(($i * $INCREMENT)))"
    echo "#        include \"cpp/list/generate/${i}.h\""
    echo "#    endif"
done)
#endif

#endif /* CPP_LIST_GENERATE_H */

