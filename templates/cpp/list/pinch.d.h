/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* elements $START to $END. */
/* this file was generated. please don't edit it */
#pragma once
#ifndef CPP_LIST_PINCH${SEQ}_H
#define CPP_LIST_PINCH${SEQ}_H

$(if [ 0 -eq $START ]; then
    echo "#define LIST_PINCH0(...) (),(__VA_ARGS__)"
    START=$(($START + 1))
fi

for i in `seq $START $END`; do
    echo "#define LIST_PINCH${i}(${ARGLIST}, ...) (${ARGLIST}),(__VA_ARGS__)"
    ARGLIST="${ARGLIST}, a${i}";
done)

#endif /* CPP_LIST_PINCH${SEQ}_H */

