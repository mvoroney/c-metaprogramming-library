/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* this file was generated. please don't edit it */
#pragma once
#ifndef CPP_LIST_PINCH_H
#define CPP_LIST_PINCH_H

#include \"cpp/util.h\" // for PASTE()
#include \"cpp/list/util.h\" // for LIST_DECAPSULATE()
#include \"cpp/list/pinch/0.h\"

#define LIST_PINCH(n, ...) LIST_PINCH_EXPAND(DEFER(1, PASTE(LIST_PINCH, n))(LIST_DECAPSULATE(__VA_ARGS__)))
#define LIST_PINCH_EXPAND(...) __VA_ARGS__

#define LIST_FRONT(n, ...) LIST_PINCH_EXPAND(DEFER(1, LIST_FRONT_I)(LIST_PINCH(n, __VA_ARGS__)))
#define LIST_FRONT_I(head, tail) LIST_PASS head

#define LIST_BACK(n, ...) LIST_PINCH_EXPAND(DEFER(1, LIST_BACK_I)(LIST_PINCH(n, __VA_ARGS__)))
#define LIST_BACK_I(head, tail) LIST_PASS tail

#ifdef LIST_MAX
$(for i in `seq 1 $(($NUM_ELEMENTS / $INCREMENT))`; do
    echo "#    if (LIST_MAX > $(($i * $INCREMENT)))"
    echo "#        include \"cpp/list/pinch/$i.h\""
    echo "#    endif"
done)
#endif

#endif /* CPP_LIST_PINCH_H */

