/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This file was generated. Please don't edit it */
#pragma once
#ifndef CPP_EXPAND_H
#define CPP_EXPAND_H

#include \"cpp/util.h\" // for PASTE
$(basedir="cpp/expand/0" symbol="EXPAND_MAX" use_template "t/dir_include.t")

#define EXPAND(level) PASTE(EXPAND, level)
#define EXPAND0(n) PASTE(EXPAND0_, n)

#ifdef EXPAND_LEVEL
$(for i in `seq 1 5`; do
    echo "#    if (EXPAND_LEVEL > $(($i - 1)))"
    echo "#        define EXPAND${i}(n) PASTE(EXPAND${i}_, n)"
    basedir="cpp/expand/${i}" symbol="EXPAND_MAX" use_template "t/dir_include.t" | sed 's/^#/#        /'
    echo "#    endif"
done)
#    if (EXPAND_LEVEL > 5)
#        error Expansion level EXPAND_LEVEL not supported.
#    endif
#endif

#endif /* CPP_EXPAND_H */

