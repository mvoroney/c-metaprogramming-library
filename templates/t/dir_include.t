
#include \"${basedir}/0.h\"

$(if [ $NUM_ELEMENTS -gt $INCREMENT ]; then

echo "#ifdef ${symbol}"

for i in `seq 1 $(($NUM_ELEMENTS / $INCREMENT - 1))`; do
    echo "#    if (${symbol} > $(($i * $INCREMENT)))"
    echo "#        include \"${basedir}/${i}.h\""
    echo "#    endif"
done

echo "#endif"

fi)
