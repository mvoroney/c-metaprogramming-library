
LIBNAME := gmp
LIB_URL := ftp://ftp.gnu.org/gnu/gmp/gmp-5.0.5.tar.xz
LIB_VERSION := 5.0.5
LIB_SUFFIX := tar.xz

BUILDDIR := $(ROOT)/dep/$(LIBNAME)
BUILDOUT := $(ROOT)/vendor
CONFIGURE_OPTS := --enable-cxx

.DEFAULT_GOAL := all
.PHONY: all
all: $(MSDIR)/$(LIBNAME).install

include lifecycle.mk
