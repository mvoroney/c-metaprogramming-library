

ifndef MSDIR
$(error MSDIR not defined, but was required in lifecycle.mk.)
endif

ifndef BUILDDIR
$(error BUILDDIR not defined, but was required in lifecycle.mk.)
endif

ifndef BUILDOUT
$(error BUILDOUT not defined, but was required in lifecycle.mk.)
endif

ifndef LIB_URL
$(error LIB_URL not defined, but was expected in lifecycle.mk.)
endif

ifndef LIB_SUFFIX
$(error LIB_SUFFIX not defined, but was expected in lifecycle.mk.)
endif

# user configurable options
CONFIGURE_OPTS := --prefix=$(BUILDOUT) $(CONFIGURE_OPTS)
#BUILD_OPTS :=
CHECK_OPTS := $(CHECK_OPTS) check
INSTALL_OPTS := $(INSTALL_OPTS) install

# user configurable commands
DOWNLOAD_CMD ?= curl -L -o $1 $2
EXTRACT_CMD ?= $(call xtar_cmd,$1,$2) 
CONFIGURE_CMD ?= cd $(BUILDDIR) && ./configure $1
BUILD_CMD ?= $(MAKE) -C $(BUILDDIR) $1
CHECK_CMD ?= $(MAKE) -C $(BUILDDIR) $1
INSTALL_CMD ?= $(MAKE) -C $(BUILDDIR) $1

# internal 
directories := $(BUILDDIR) $(BUILDOUT) $(MSDIR)
lifecycle := download extract configure build check install

xtar_flag = $(patsubst $1:%,%,$(filter $1:%,tar.gz:z tgz:z tar.xz:J tar.bz2:j))
xtar_cmd = tar -x$(call xtar_flag,$2)f $1.$2 -C $(BUILDDIR) --transform 's@^\(./\)\{0,1\}[^/]*$1[^/]*/@@'

# ensure required directories exist.
$(foreach d,$(filter-out $(wildcard $(directories)),$(directories)),$(shell mkdir $d))

# setup vpaths for milestones
$(foreach phase,$(lifecycle),$(eval vpath %.$(phase) $(MSDIR)))

.SECONDARY: $(patsubst %,$(MSDIR)/$(LIBNAME).%,$(lifecycle))

# Build Stages

%.download:
	$(call DOWNLOAD_CMD,dep/$(*F).$(LIB_SUFFIX),$(LIB_URL))
	@touch $@

%.extract: %.download
	$(call EXTRACT_CMD,dep/$(*F),$(LIB_SUFFIX))
	@touch $@

%.configure: %.extract
	$(call CONFIGURE_CMD,$(CONFIGURE_OPTS))
	@touch $@

%.build: %.configure
	$(call BUILD_CMD,$(BUILD_OPTS))
	@touch $@

%.check: %.build
	$(call CHECK_CMD,$(CHECK_OPTS))
	@touch $@

%.install: %.check
	$(call INSTALL_CMD,$(INSTALL_OPTS))
	@touch $@

