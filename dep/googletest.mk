
LIBNAME := googletest
LIB_VERSION := release-1.8.0
LIB_SUFFIX := zip
LIB_URL := https://github.com/google/googletest/archive/$(LIB_VERSION).$(LIB_SUFFIX)

BUILDDIR := $(ROOT)/dep/$(LIBNAME)
BUILDOUT := $(ROOT)/vendor

define EXTRACT_CMD
	rm -rf $(BUILDDIR) $(BUILDDIR)-$(LIB_VERSION)
	unzip $1.$2 -d $(ROOT)/dep
	mv -f $(BUILDDIR)-$(LIB_VERSION) $(BUILDDIR)
endef
CONFIGURE_CMD = 
define BUILD_CMD
	$(MAKE) -C $(BUILDDIR)/googletest/make $1
	$(MAKE) -C $(BUILDDIR)/googlemock/make $1
endef
define CHECK_CMD
	cd $(BUILDDIR)/googletest/make && ./sample1_unittest
	cd $(BUILDDIR)/googlemock/make && ./gmock_test
endef
define INSTALL_CMD
	@echo Installing Google Test to $(BUILDOUT)
	@mkdir -p $(BUILDOUT)/include $(BUILDOUT)/lib
	@cp -rf $(BUILDDIR)/googletest/include/* $(BUILDOUT)/include/
	@cp -f $(BUILDDIR)/googletest/make/gtest_main.a $(BUILDOUT)/lib/libgtest_main.a
	@echo Installing Google Mock to $(BUILDOUT)
	@cp -rf $(BUILDDIR)/googlemock/include/* $(BUILDOUT)/include/
	@cp -f $(BUILDDIR)/googlemock/make/gmock_main.a $(BUILDOUT)/lib/libgmock_main.a
endef

.DEFAULT_GOAL := all
.PHONY: all
all: $(MSDIR)/$(LIBNAME).install

include lifecycle.mk

