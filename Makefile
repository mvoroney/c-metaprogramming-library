
PROJECT_NAME := C++ Meta 
PROJECT_VERSION := $(shell git describe)
PROJECT_BRIEF := A C++ preprocessor and template metaprogramming library.

PROGNAME := cppmeta

ROOT := $(abspath .)
MSDIR := $(ROOT)/.msdir
OBJDIR := obj
BINDIR := bin
DOCDIR := doc
LOGDIR := log
DEPINSTALL := vendor

CXX := g++
CXXFLAGS := -Wall -Wextra -ansi -pedantic -std=c++0x -O3 -g -pthread
INCLUDE := $(DEPINSTALL)/include include/generated include test
CPPFLAGS := $(patsubst %,-I%,$(INCLUDE))
LIB := $(LIBDIR) $(DEPINSTALL)/lib
LDFLAGS := $(patsubst %,-L%,$(LIB))

DOXYGEN := $(if $(wildcard /usr/local/bin/doxygen),/usr/local/bin/doxygen,doxygen)

srcdirs := src include test
directories := $(OBJDIR) $(LIBDIR) $(BINDIR) $(MSDIR) $(DOCDIR) $(LOGDIR)
dependencies := googletest
lifecycle := depend generate build test htmldoc package

$(PROGNAME)_INC := $(notdir $(wildcard include/*.h))
$(PROGNAME)_GEN := $(patsubst templates/%.d.h,%,$(patsubst templates/%.f.h,%.h,$(shell find templates -name *.h)))
$(PROGNAME)_test_SRC := $(notdir $(wildcard test/*.cc))
$(PROGNAME)_test_INC := $(notdir $(wildcard test/*.h))
$(PROGNAME)_test_OBJ := $($(PROGNAME)_test_SRC:.cc=.o)

have_dot := $(if $(shell which dot 2> /dev/null),YES,NO)
compile = $(CXX) $(CPPFLAGS) $(CXXFLAGS) $(pchflags) -c -x c++ $< -o $@ -MMD -MF $(@:.o=.d)
link = $(CXX) $(LDFLAGS) $(CPPFLAGS) $(CXXFLAGS) $(filter %.o,$^) -o $@

# setup vpaths for milestones
$(foreach phase,$(lifecycle),$(eval vpath %.$(phase) $(BUILDROOT)/$(MSDIR)))

.DEFAULT_GOAL := all
.PHONY: all depend test doc clean generate 
all: test
depend: $(MSDIR)/$(PROGNAME).depend
test: $(MSDIR)/$(PROGNAME).test
doc: $(MSDIR)/$(PROGNAME).htmldoc
generate: $(MSDIR)/$(PROGNAME).generate

$(MSDIR)/$(PROGNAME).depend: $(patsubst %,$(MSDIR)/%.install,$(dependencies))
	@echo Dependencies were installed successfully.
	@touch $@

$(MSDIR)/$(PROGNAME).generate: $(patsubst %,include/generated/%,$($(PROGNAME)_GEN))

$(MSDIR)/$(PROGNAME).test: $(MSDIR)/$(PROGNAME).generate $(BINDIR)/$(PROGNAME).test

$(patsubst %,$(MSDIR)/%.install,$(dependencies)): %.install:
	$(MAKE) MSDIR=$(MSDIR) ROOT=$(ROOT) -Idep -f dep/$(*F).mk

$(BINDIR)/$(PROGNAME).test: $(addprefix $(OBJDIR)/,$($(PROGNAME)_test_OBJ))
	$(link) -lgmock_main

$(OBJDIR)/%.o: src/%.cc 
	$(compile)

$(OBJDIR)/%.o: test/%.cc
	$(compile)

include/generated/%: templates/%.d.h
	scripts/poop.sh include/generated/$*=templates/$*

include/generated/%.h: templates/%.f.h
	scripts/poop.sh include/generated/$*=templates/$*

%.test: %.depend
	LD_LIBRARY_PATH=$(DEPINSTALL)/lib $(BINDIR)/$(@F) --gtest_output=xml:$(LOGDIR)/$(@F).xml
	@echo Testing was run successfully.
	@touch $@

clean:
	rm -rf $(filter-out $(MSDIR),$(directories)) $(addprefix $(MSDIR)/$(PROGNAME).,$(lifecycle)) include/generated doc

depclean: 
	rm -rf $(filter-out $(wildcard dep/*.mk),$(wildcard dep/*)) $(MSDIR)/*

# variable exports needed for doxygen
export ROOT have_dot srcdirs DOCDIR INCLUDE PROJECT_NAME PROJECT_VERSION PROJECT_BRIEF

%.htmldoc: Doxyfile
	$(DOXYGEN) $<
	@touch $@

ifneq ($(MAKECMDGOALS),clean)
include $(wildcard $(OBJDIR)/*.d)

# ensure required directories exist.
$(foreach d,$(filter-out $(wildcard $(directories)),$(directories)),$(shell mkdir $d))
endif
