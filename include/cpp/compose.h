/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_COMPOSE_H
#define CPP_COMPOSE_H

#include "cpp/util.h"
#include "cpp/logic.h"
#include "cpp/integer.h"
#include "cpp/list/util.h"
#include "cpp/placeholder.h"

#ifndef NO_DEFAULT_EXPANDER
#   include "cpp/expand.h"
#endif

#ifdef WITH_RIGHT_COMPOSE
#   include "cpp/list/fold.h"
#endif

/**
 * @private
 *
 * Function composition generic back end
 *
 * @{
 */
#define COMPOSEL_I(expand, level, direction, ...) expand(level)(0)(COMPOSE_I(expand, level, PREV(LIST_LENGTH(__VA_ARGS__)), direction, __VA_ARGS__, ~))

#define COMPOSE_I(expand, level, state, direction, ...) IIF(state)(COMPOSE_NEXT, COMPOSE_ACCUMULATOR)(expand, level, state, direction, __VA_ARGS__)

#define COMPOSE_INDIRECT() COMPOSE_I

#define COMPOSE_NEXT(expand, level, state, direction, accumulator, mspec1, ...) DEFER(1, expand(level)(state))(DEFER(1, COMPOSE_INDIRECT)()(expand, level, PREV(state), direction, COMPOSE_EVALM(accumulator, mspec1), __VA_ARGS__))

#define COMPOSE_EVALM(vals, mspec) (PLACEHOLDER_EVALM(FIRST mspec, LIST_LENGTH SECOND mspec, vals, SECOND mspec), COMPOSE_VALS2 vals)
#define COMPOSE_ACCUMULATOR(expand, level, state, direction, accumulator, ...) ENCAPSULATE(accumulator)
#define COMPOSE_VALS2(v1, ...) __VA_ARGS__ 
#define COMPOSE_EXPAND(...) __VA_ARGS__
/** @} */

/**
 * Completely generic function composition front end. Allows the iterative
 * expander, expansion level, and associativeity direction to be specified.
 *
 * @note this is primarily useful for library writers who need to make sure
 * that their use of function composition macros do not colide with other
 * user level iterative macro expansions.
 *
 * @todo document the requirements for 3rd party expanders and link from here.
 */
#define COMPOSELE(expand, level, direction, ...) COMPOSE_EXPAND(FIRST COMPOSEL_I(expand, level, direction, __VA_ARGS__))

/**
 * Successively combines the elements of the list in left to right order
 * according to the supplied macro.
 *
 * @param macro - The macro to use to combine list elements.
 * @param args  - The argument list to be supplied to the macro. Placeholder
 *                expansion is peformed on the list prior to invocation of the
 *                macro.
 *                - _1 expands to the accumulated value.
 *                - _2 expands to the current list value.
 *                - _3 expands to the nesting level the macro was invoked at.
 * @param accum - A starting value for the accumulator. If the supplied
 *                list is empty, this will be returned as the default value.
 * 
 * @return The result of combining all of the elements in the list according
 *         to the supplied macro.
 */
#define LEFT_COMPOSE(...) LEFT_COMPOSEL(0, __VA_ARGS__)
#define LEFT_COMPOSE0(...) LEFT_COMPOSEL(0, __VA_ARGS__)
#define LEFT_COMPOSE1(...) LEFT_COMPOSEL(1, __VA_ARGS__)
#define LEFT_COMPOSE2(...) LEFT_COMPOSEL(2, __VA_ARGS__)
#define LEFT_COMPOSE3(...) LEFT_COMPOSEL(3, __VA_ARGS__)
#define LEFT_COMPOSE4(...) LEFT_COMPOSEL(4, __VA_ARGS__)
#define LEFT_COMPOSE5(...) LEFT_COMPOSEL(5, __VA_ARGS__)

/**
 * Left associative function composition with expansion level
 *
 * @see RIGHT_COMPOSEL() A right associative variant.
 */
#define LEFT_COMPOSEL(level, ...) COMPOSELE(EXPAND, level, LEFT, LIST_DECAPSULATE(__VA_ARGS__))

#ifdef LIST_REVERSEL

#define RIGHT_COMPOSE(...) RIGHT_COMPOSEL(0, __VA_ARGS__)
#define RIGHT_COMPOSE0(...) RIGHT_COMPOSEL(0, __VA_ARGS__)
#define RIGHT_COMPOSE1(...) RIGHT_COMPOSEL(1, __VA_ARGS__)
#define RIGHT_COMPOSE2(...) RIGHT_COMPOSEL(2, __VA_ARGS__)
#define RIGHT_COMPOSE3(...) RIGHT_COMPOSEL(3, __VA_ARGS__)
#define RIGHT_COMPOSE4(...) RIGHT_COMPOSEL(4, __VA_ARGS__)
#define RIGHT_COMPOSE5(...) RIGHT_COMPOSEL(5, __VA_ARGS__)

/**
 * Right associative function composition with expansion level 
 *
 * @see LEFT_COMPOSEL() A left associative variant.
 */
#define RIGHT_COMPOSEL(level, ...) COMPOSE_EXPAND(FIRST COMPOSEL_I(EXPAND, level, RIGHT, LIST_REVERSEL(level, COMMA, __VA_ARGS__)))

#else
#pragma message("LIST_REVERSEL not detected. RIGHT_COMPOSE will be unavailable.")
#pragma message("#include \"cpp/list/fold.h\" or define WITH_RIGHT_COMPOSE before including cpp/compose.h to avoid this message.")
#endif /** LIST_REVERSEL */

#endif /* CPP_COMPOSE_H */

