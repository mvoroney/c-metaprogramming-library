/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPP_VERTICAL_REPEAT_H
#define CPP_VERTICAL_REPEAT_H

#   include "cpp/value/transfer.h"
#   include "cpp/util.h"

#   define VERTICAL_REPEAT_MIN_(a, b) a
#   define VERTICAL_REPEAT_MAX_(a, b) b
#   define VERTICAL_REPEAT_LOOP() "cpp/vertical/repeat.h"

#else /* CPP_VERTICAL_REPEAT_H */

#   ifndef VERTICAL_REPEAT_COUNTER

#       ifndef VERTICAL_REPEAT_LIMITS
#           error "VERTICAL_REPEAT_LIMITS not defined: expected (min, max)"
#       endif

#       ifndef VERTICAL_REPEAT_FILE
#           error "VERTICAL_REPEAT_FILE not defined"
#       endif

#       define VERTICAL_REPEAT_COUNTER EXPR(VERTICAL_REPEAT_MIN_ VERTICAL_REPEAT_LIMITS)
#       define VERTICAL_REPEAT_IS_ITERATING 1

#   endif /* VERTICAL_REPEAT_COUNTER */

#   if (VERTICAL_REPEAT_COUNTER) < (EXPR(VERTICAL_REPEAT_MAX_ VERTICAL_REPEAT_LIMITS))

#       include VERTICAL_REPEAT_FILE

#       define VALUE_TRANSFER_IN_1 VERTICAL_REPEAT_COUNTER + 1
#       include VALUE_TRANSFER_1()
#       undef VERTICAL_REPEAT_COUNTER
#       define VERTICAL_REPEAT_COUNTER VALUE_TRANSFER_OUT_1

#       include VERTICAL_REPEAT_LOOP()

#   else /* LOOP CONDITION */

#       undef VERTICAL_REPEAT_IS_ITERATING
#       undef VERTICAL_REPEAT_COUNTER
#       undef VALUE_TRANSFER_OUT_1

#   endif /* LOOP CONDITION */
#endif /* CPP_VERTICAL_REPEAT_H */
