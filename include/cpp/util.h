/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_UTILS_H
#define CPP_UTILS_H

#define PASTE(x, y) PASTE_(x, y)
#define PASTE_(x, y) x ## y
#define STRINGIZE(...) STRINGIZE_(__VA_ARGS__)
#define STRINGIZE_(...) #__VA_ARGS__

#define PASTEN(n, ...) PASTE(PASTE, n) (__VA_ARGS__)
#define PASTE1(...) FIRST(__VA_ARGS__, ~)
#define PASTE2(x, ...) PASTE(x, FIRST(__VA_ARGS__, ~))
#define PASTE3(x, ...) PASTE(x, PASTE2(__VA_ARGS__))
#define PASTE4(x, ...) PASTE(x, PASTE3(__VA_ARGS__))
#define PASTE5(x, ...) PASTE(x, PASTE4(__VA_ARGS__))
#define PASTE6(x, ...) PASTE(x, PASTE5(__VA_ARGS__))
#define PASTE7(x, ...) PASTE(x, PASTE6(__VA_ARGS__))
#define PASTE8(x, ...) PASTE(x, PASTE7(__VA_ARGS__))
#define PASTE9(x, ...) PASTE(x, PASTE8(__VA_ARGS__))

/**
 * @private
 * Extracts the first element of the supplied list.
 */
#define FIRST(...) FIRST_(__VA_ARGS__, ~)
#define FIRST_(a, ...) a

/**
 * @private
 * Extracts the second element of the supplied list.
 */
#define SECOND(a, ...) FIRST(__VA_ARGS__)


#define BLANK()
#define COMMA() ,
#define LPAREN() (
#define RPAREN() )

/**
 * Delays the expansion of a function like macro for n scans.
 */
#define DEFER(n, ...) PASTE(DEFER, n)(__VA_ARGS__)
#define DEFER0(...) __VA_ARGS__
#define DEFER1(...) __VA_ARGS__ BLANK()
#define DEFER2(...) __VA_ARGS__ DEFER1(BLANK)()
#define DEFER3(...) __VA_ARGS__ DEFER2(BLANK)()
#define DEFER4(...) __VA_ARGS__ DEFER3(BLANK)()
#define DEFER5(...) __VA_ARGS__ DEFER4(BLANK)()

#endif /* CPP_UTILS_H */
