/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_PLACEHOLDER_H
#define CPP_PLACEHOLDER_H

#include "cpp/util.h"
#include "cpp/logic.h"

/**
 * @private
 *
 * Simply adds an extra element to the list to prevent C99 rest-args warnings.
 */
#define ARGLIST_PAD(...) (__VA_ARGS__, ~, ~)

/**
 * @private
 *
 * Binds the supplied argument to one of the values if the argument was a
 * placeholder expression such as _1, _2, ..., _9. Otherwise the original
 * argument token is passed through.
 *
 * @param arg  - The argument to attempt value substitution for.
 * @param vals - The list of values that can be selected by arg, if arg is a
 *               placeholder. vals is expected to be parenthetically encapsulated.
 *
 * @return The indexed value from vals if arg was a placeholder. arg otherwise.
 */
#define PLACEHOLDER_EVAL(arg, vals) PASTE(PLACEHOLDER_EVAL_I, ISPARENTHETICAL(arg))(arg, vals)
#define PLACEHOLDER_EVAL_I0(arg, vals) PLACEHOLDER_EVAL_EXPAND(DEFER(1, SECOND)(PASTE(PLACEHOLDER_EVAL, arg) vals, arg))
#define PLACEHOLDER_EVAL_I1(arg, vals) arg
#define PLACEHOLDER_EVAL_EXPAND(...) __VA_ARGS__

/**
 * @private
 *
 * Placeholder value extractors.
 * @{
 */
#define PLACEHOLDER_EVAL_1(v1, ...) ~,v1
#define PLACEHOLDER_EVAL_2(v1, v2, ...) ~,v2
#define PLACEHOLDER_EVAL_3(v1, v2, v3, ...) ~,v3
#define PLACEHOLDER_EVAL_4(v1, v2, v3, v4, ...) ~,v4
#define PLACEHOLDER_EVAL_5(v1, v2, v3, v4, v5, ...) ~,v5
#define PLACEHOLDER_EVAL_6(v1, v2, v3, v4, v5, v6, ...) ~,v6
#define PLACEHOLDER_EVAL_7(v1, v2, v3, v4, v5, v6, v7, ...) ~,v7
#define PLACEHOLDER_EVAL_8(v1, v2, v3, v4, v5, v6, v7, v8, ...) ~,v8
#define PLACEHOLDER_EVAL_9(v1, v2, v3, v4, v5, v6, v7, v8, v9, ...) ~,v9
/** @} */

/**
 * @private
 *
 * Placeholder argument extractors.
 * @{
 */
#define GET_ARG1(a1, ...) a1
#define GET_ARG2(a1, a2, ...) a2
#define GET_ARG3(a1, a2, a3, ...) a3
#define GET_ARG4(a1, a2, a3, a4, ...) a4
#define GET_ARG5(a1, a2, a3, a4, a5, ...) a5
#define GET_ARG6(a1, a2, a3, a4, a5, a6, ...) a6
#define GET_ARG7(a1, a2, a3, a4, a5, a6, a7, ...) a7
#define GET_ARG8(a1, a2, a3, a4, a5, a6, a7, a8, ...) a8
#define GET_ARG9(a1, a2, a3, a4, a5, a6, a7, a8, a9, ...) a9
/** @} */

/**
 * Performs all placeholder substitions in the argument list with one of the
 * supplied values. A placeholder expression is one of _1, _2, ..., _9.
 * Each will be replaced with the first, second, ..., ninth element of the
 * value list.
 *
 * @param n    - The number of elements to replace in the argument list args
 * @param vals - The set of values to replace the corresponding argument
 *               placeholder expression with.
 * @param args - The argument list to perform substitution on.
 *
 * @return A non-parenthetically encapsulated list with all placeholder
 *         substitutions performed.
 */
#define PLACEHOLDER_EVALN(n, vals, args) PLACEHOLDER_EVALN_EXPAND(DEFER(1, PASTE(PLACEHOLDER_EVALN, n))(ARGLIST_PAD vals, ARGLIST_PAD args))
#define PLACEHOLDER_EVALN_EXPAND(...) __VA_ARGS__

#define PLACEHOLDER_EVALN0(vals, args)
#define PLACEHOLDER_EVALN1(vals, args) PLACEHOLDER_EVAL(GET_ARG1 args, vals)
#define PLACEHOLDER_EVALN2(vals, args) PLACEHOLDER_EVALN1(vals, args), PLACEHOLDER_EVAL(GET_ARG2 args, vals)
#define PLACEHOLDER_EVALN3(vals, args) PLACEHOLDER_EVALN2(vals, args), PLACEHOLDER_EVAL(GET_ARG3 args, vals)
#define PLACEHOLDER_EVALN4(vals, args) PLACEHOLDER_EVALN3(vals, args), PLACEHOLDER_EVAL(GET_ARG4 args, vals)
#define PLACEHOLDER_EVALN5(vals, args) PLACEHOLDER_EVALN4(vals, args), PLACEHOLDER_EVAL(GET_ARG5 args, vals)
#define PLACEHOLDER_EVALN6(vals, args) PLACEHOLDER_EVALN5(vals, args), PLACEHOLDER_EVAL(GET_ARG6 args, vals)
#define PLACEHOLDER_EVALN7(vals, args) PLACEHOLDER_EVALN6(vals, args), PLACEHOLDER_EVAL(GET_ARG7 args, vals)
#define PLACEHOLDER_EVALN8(vals, args) PLACEHOLDER_EVALN7(vals, args), PLACEHOLDER_EVAL(GET_ARG8 args, vals)
#define PLACEHOLDER_EVALN9(vals, args) PLACEHOLDER_EVALN8(vals, args), PLACEHOLDER_EVAL(GET_ARG9 args, vals)

/**
 * Evaluates a macro with placeholder expansion applied to it's argument list,
 * with the supplied values. A placeholder expression is one of _1, _2, ..., _9. 
 * Where the digit indicates the index of the value to substitute from
 * the supplied value list.
 *
 * @param macro - The macro to be evaluated.
 * @param n     - The number of arguments that the argument list args contains.
 * @param vals  - The set of values to map to placeholder expressions.
 * @param args  - The argument list for the supplied macro, which may or may
 *                not contain placeholder expressions to perform substitution on.
 *
 * @return The result of evaluating the supplied macro against the supplied
 *         argument list with the set of specified value substitutions.
 */
#define PLACEHOLDER_EVALM(macro, n, vals, args) PLACEHOLDER_EVALM_EXPAND(DEFER(1, macro)(PLACEHOLDER_EVALN(n, vals, args)))
#define PLACEHOLDER_EVALM_EXPAND(...) __VA_ARGS__

#endif /* CPP_PLACEHOLDER_H */

