/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * This file contains the implementation of a repetition macro.
 *
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_HORIZONTAL_REPEAT_H
#define CPP_HORIZONTAL_REPEAT_H

#include "cpp/util.h"
#include "cpp/list/map.h"
#include "cpp/list/generate.h"

#ifndef NO_DEFAULT_EXPANDER
#   include "cpp/expand.h"
#endif

/**
 * Allow a macro to be repeatedly expanded up to the limit of LIST_MAPLE().
 * Placeholder expansion will be performed on the macro argument list args.
 * This is just a simple wrapper around LIST_MAPLE().
 * 
 * @param n     - The number of times to expand the supplied macro.
 * @param macro - a macro to be expanded n times.
 * @param sep   - the name of a no-arg function macro, to be placed between
 *                expansions of macro
 *
 * @return A list of the results of successively invoking the supplied macro,
 *         with the current invocation mapped to the _1 placeholder argument.
 *
 * @see PLACEHOLDER_EVAL for more information on how placeholder argument
 *      expansion works.
 * @{
 */
#define HORIZONTAL_REPEAT(n, macro, args, sep) HORIZONTAL_REPEATL(0, n, macro, args, sep)
/**
 * @param level - The nesting level this macro was invoked at.
 * @see HORIZONTAL_REPEAT() for a description of the remaining parameters.
 */
#define HORIZONTAL_REPEATL(level, n, macro, args, sep) HORIZONTAL_REPEATLE(EXPAND, level, n, macro, args, sep)
/**
 * @param expand - The expander to use for iteration.
 * @see HORIZONTAL_REPEATL() for a description of the remaining parameters.
 */
#define HORIZONTAL_REPEATLE(expand, level, n, macro, args, sep) LIST_MAPLE(expand, level, macro, args, sep, LIST_GENERATE(n))
/** @} */

#endif /* CPP_HORIZONTAL_REPEAT_H */

