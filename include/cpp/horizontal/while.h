/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_HORIZONTAL_WHILE_H
#define CPP_HORIZONTAL_WHILE_H

#include "cpp/integer.h"
#include "cpp/logic.h"
#include "cpp/placeholder.h"
#include "cpp/list/util.h"
#include "cpp/expand.h"

#define HORIZONTAL_WHILE(cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL(0, cond, cargs, macro, margs, sep, __VA_ARGS__)
#define HORIZONTAL_WHILE0(cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL(0, cond, cargs, macro, margs, sep, __VA_ARGS__)
#define HORIZONTAL_WHILE1(cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL(1, cond, cargs, macro, margs, sep, __VA_ARGS__)
#define HORIZONTAL_WHILE2(cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL(2, cond, cargs, macro, margs, sep, __VA_ARGS__)
#define HORIZONTAL_WHILE3(cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL(3, cond, cargs, macro, margs, sep, __VA_ARGS__)
#define HORIZONTAL_WHILE4(cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL(4, cond, cargs, macro, margs, sep, __VA_ARGS__)
#define HORIZONTAL_WHILE5(cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL(5, cond, cargs, macro, margs, sep, __VA_ARGS__)

#define HORIZONTAL_WHILEL(level, cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILELE(EXPAND, level, cond, cargs, macro, margs, sep, __VA_ARGS__) 

#define HORIZONTAL_WHILELE(expand, level, cond, cargs, macro, margs, sep, ...)  HORIZONTAL_WHILEL_I(expand, level, cond, cargs, macro, margs, sep, LIST_DECAPSULATE(__VA_ARGS__))

#define HORIZONTAL_WHILEL_I(expand, level, cond, cargs, macro, margs, sep, ...) HORIZONTAL_WHILEL_II(expand, level, LIST_LENGTH(__VA_ARGS__), cond, LIST_LENGTH cargs, cargs, macro, LIST_LENGTH margs, margs, sep, __VA_ARGS__, ~, ~)

#define HORIZONTAL_WHILEL_II(expand, level, state, cond, cn, cargs, macro, mn, margs, sep, a1, ...) expand(level)(0)(IIF(AND(state, PLACEHOLDER_EVALM, (cond, cn, (a1, level, ~), cargs)))(PLACEHOLDER_EVALM(macro, mn, (a1, level, ~), margs)HORIZONTAL_WHILE_I, LIST_EAT)(expand, level, PREV(state), cond, cn, cargs, macro, mn, margs, sep, __VA_ARGS__))

#define HORIZONTAL_WHILE_I(expand, level, state, cond, cn, cargs, macro, mn, margs, sep, a1, ...) IIF(AND(state, PLACEHOLDER_EVALM, (cond, cn, (a1, level, ~), cargs)))(HORIZONTAL_WHILE_NEXT, LIST_EAT)(expand, level, state, cond, cn, cargs, macro, mn, margs, sep, a1, __VA_ARGS__)

#define HORIZONTAL_WHILE_INDIRECT() HORIZONTAL_WHILE_I

#define HORIZONTAL_WHILE_NEXT(expand, level, state, cond, cn, cargs, macro, mn, margs, sep, a1, ...) sep()PLACEHOLDER_EVALM(macro, mn, (a1, level, ~), margs)DEFER(1, expand(level)(state))(DEFER(1, HORIZONTAL_WHILE_INDIRECT)() (expand, level, PREV(state), cond, cn, cargs, macro, mn, margs, sep, __VA_ARGS__)) 

#endif /* CPP_HORIZONTAL_WHILE_H */

