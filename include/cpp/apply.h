/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * Contains an implementation of an apply macro, allowing an arbitrary macro to
 * be composed an arbitrary number of times with itself with flexible mappings.
 *
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_APPLY_H
#define CPP_APPLY_H

#include "cpp/util.h"
#include "cpp/logic.h"
#include "cpp/list/util.h"
#include "cpp/list/map.h"
#include "cpp/compose.h"
#include "cpp/list/generate.h"

#ifndef NO_DEFAULT_EXPANDER
#   include "cpp/expand.h"
#endif

/**
 * new version of apply using function composition operator. This is a
 * convenience operator that generates a list containing n copies of the
 * supplied macro specification (mspec), and then uses the function composition
 * operator COMPOSELE() to accumulate the result of applying the list of macro
 * specifications to the supplied args.
 * 
 * @param expand - The name of the expansion backend to use.
 *
 * @param level - The nesting level this macro is invoked at. This parameter
 *  should be 0 unless it is invoked iteratively by another iterative macro
 *  using the same expansion backend. Then it should be l + 1, where l is the
 *  nesting level of the outer iterative macro.
 *
 * @param n - The number of times the supplied macro specification (mspec)
 *  should be composed with itself.
 *
 * @param mspec - The macro specification for the macro to be applied. This
 *  includes the macro followed by it's parameter mappings.
 * @verbatim
 *  (<macro>, (<parameters>))
 * @endverbatim
 *  Where <parameters> can contain literal values and placeholder expressions
 *
 * @param args - The argument list for the first invocation of the applied macro.
 *
 * @see PLACEHOLDER_EVAL
 * @see PLACEHOLDER_EVALM
 * @see COMPOSELE() The operator this macro is implemented in terms of.
 * @{
 */
#define APPLY(n, mspec, args) APPLYL(0, n, mspec, args)
#define APPLYL(level, n, mspec, args) APPLYLE(EXPAND, level, n, mspec, args)
#define APPLYLE(expand, level, n, mspec, args) PASTE(APPLYL_, ISZERO(n))(expand, level, n, mspec, args)
/** @} */

/**
 * @private
 * @{
 */
#define APPLYL_0(expand, level, n, mspec, args) APPLYL_I(expand, level, n, mspec, args, LIST_GENERATE(n))
#define APPLYL_1(expand, level, n, mspec, args) FIRST args
#define APPLYL_I(expand, level, n, mspec, args, ...) COMPOSELE(expand, level, LEFT, args, LIST_MAPLE(expand, level, LIST_PASS, (mspec), COMMA, __VA_ARGS__))
/** @} */

#endif /* CPP_APPLY_H */

