/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * @author Matt Voroney
 */
#ifndef CPP_LOGIC_H
#define CPP_LOGIC_H

#include "cpp/util.h"

/**
 * Checks to see if the supplied expression was encapsulated in parentheses.
 *
 * @warning This macro is not list safe.
 *
 * @param x - Any non-list expression.
 *
 * @retval 1 Iff the supplied expression was encapsulated in parentheses.
 * @retval 0 Iff the supplied expression was not encapsulated in parentheses.
 *
 * @see LIST_ISPARENTHETICAL() A list safe variant of this macro.
 */
#define ISPARENTHETICAL(x) ISPARENTHETICAL_EXPAND(DEFER(1, FIRST)(PASTE(ISPARENTHETICAL_I_, ISPARENTHETICAL_II x)))
#define ISPARENTHETICAL_EXPAND(...) __VA_ARGS__
#define ISPARENTHETICAL_I_ISPARENTHETICAL_II 0, ~
#define ISPARENTHETICAL_II(...) 1
#define ISPARENTHETICAL_I_1 1, ~

/**
 * Removes the outermost pair of parentheses of a parenthetical expression.
 * 
 * @warning This macro is not list safe.
 * 
 * @param x - Any non-list expression.
 *
 * @return The supplied expression with the outermost set of parentheses
 *         removed, if there were any.
 * 
 * @see LIST_DECAPSULATE() A list safe variant of this macro.
 */
#define DECAPSULATE(x) PASTE(DECAPSULATE_, ISPARENTHETICAL(x))(x)
#define DECAPSULATE_1(x) DECAPSULATE_I x
#define DECAPSULATE_I(...) __VA_ARGS__
#define DECAPSULATE_0(x) x

/**
 * Ensures that the supplied expression is parenthetically encapsulated, but
 * does not add any further encapsulation if not necessary.
 *
 * @warning This macro is not list safe.
 *
 * @param x - The expression to ensure the encapsulation of.
 *
 * @retval x Iff x was already a parenthetical expression.
 * @retval (x) Iff x was not already a parenthetical expression.
 *
 * @see ISPARENTHETICAL() For information on what a parenthetical expression
 *      is.
 * @see DECAPSULATE() The dual of this macro.
 * @see LIST_ENCAPSULATE() A list safe variant of this macro.
 */
#define ENCAPSULATE(x) PASTE(ENCAPSULATE_, ISPARENTHETICAL(x))(x)
#define ENCAPSULATE_1(x) x
#define ENCAPSULATE_0(x) (x)

/**
 * @private
 *
 * Should work for any non-parenthetical expression that does not include
 * commas. 
 * 
 * @warning This macro is not parenthetical expression safe.
 * 
 * @param x - Any non-parenthetical expression. Only '0' is considered false.
 *            Any non-zero expression is considered true.
 *
 * @retval 0 Iff x was not 0.
 * @retval 1 Iff x was 0.
 *
 * @see ISZERO() A higher level '0' test predicate. This one accepts
 *               parenthetical expressions as well. Use it instead.
 */
#define ISZERO_E(x) ISZERO_EXPAND(DEFER(1, SECOND)(PASTE(ISZERO_E, x), 0))
#define ISZERO_EXPAND(...) __VA_ARGS__
#define ISZERO_E0 ~, 1

/**
 * Tests to see if the supplied expression was zero or not.
 *
 * @warning This macro is not list safe.
 *
 * @param x - Any non-list expression. Only '0' is considered true. Any
 *            non-zero expression is considered to be false.
 *
 * @retval 1 Iff x was 0.
 * @retval 0 Iff x was not 0.
 */
#define ISZERO(x) PASTE(ISZERO_P, ISPARENTHETICAL(x))(x)
#define ISZERO_P1(x) 0
#define ISZERO_P0(x) ISZERO_E(x)

/**
 * Tests to see if the supplied expression was a blank or not.
 * i.e., contains no tokens.
 *
 * @warning This macro is not list safe.
 *
 * @param x - Any non-list expression. Only '' is considered blank. Any other
 *            expression is considered to be non-blank.
 *
 * @retval 1 Iff x was blank (contained no tokens).
 * @retval 0 Iff x contained any tokens.
 */
#define ISBLANK(x) ISBLANK_EXPAND(DEFER(1, SECOND)(PASTE(BLANK_ x, EXTENSION), 0))
#define ISBLANK_EXPAND(...) __VA_ARGS__
#define BLANK_(...) NON_BLANK_
#define NON_BLANK_EXTENSION ~, 0
#define BLANK_EXTENSION ~, 1

/**
 * Tests to see if the supplied expression was an empty set of parentheses.
 *
 * @warning This macro is not list safe.
 *
 * @param x - Any non-list expression. only '()' is considered empty. Any other
 *            expression is considred non-empty.
 *
 * @retval 1 Iff x was empty.
 * @retval 0 Iff x was non-empty.
 */
#define ISEMPTY(x) ISEMPTY_EXPAND(DEFER(1, FIRST)(PASTE(ISEMPTY_, ISBLANK x)))
#define ISEMPTY_EXPAND(...) __VA_ARGS__
#define ISEMPTY_1 1, ~
#define ISEMPTY_0 0, ~
#define ISEMPTY_ISBLANK 0, ~ 

/**
 * @private
 *
 * Used for short circuit logical evaluation.
 *
 * @param macro - The macro or token(s) to evaluate for truth value.
 * @param args  - The argument list to supply to the provided macro. This
 *                argument is optional, but if supplied is expected to be a
 *                parenthetical list. Iff this argument is blank, then the
 *                supplied macro will be evaluated as an expression.
 *
 * @retval ISTRUE(macro) Iff ISBLANK(args) == 1.
 * @retval ISTRUE(macro(args)) Iff ISBLANK(args) == 0.
 */
#define LOGIC_EVALM(macro, args, ...) PASTE(LOGIC_EVALM_, ISBLANK(args))(macro, args)
#define LOGIC_EVALM_1(macro, args) ISTRUE(macro)
#define LOGIC_EVALM_0(macro, args) ISTRUE(macro args)

/**
 * @defgroup cpp_logical_ops Logical Operators
 * 
 * Each logical operator invokes ISTRUE() on each of it's arguments immediately
 * before the argument is evaulated. Evaluation of the second argument is
 * avoided altoghether if possible. Logical operators accepting more than one
 * argument are built to perform lazy short circuit evaluation of it's second
 * argument if necessary. To Invoke the operator with immediate evaluation of
 * both of it's arguments, simply use the two-arg form:
 *
 * \code
 * OR(1, 0);
 * AND(1, 2);
 * IMPLIES(0, 1);
 * \endcode
 *
 * If you require lazy evaluation on the second argument, use the three arg
 * form:
 *
 * \code
 * OR(0, FOO, (foo, args));
 * AND(1, BAR, (bar, args));
 * IMPLIES(1, BAZ, (baz, args));
 * \endcode
 *
 * If the third argument is supplied, it must be a parenthetical list. The
 * result is undefined otherwise.
 *
 * @param x - The first argument to the logical connective.
 * @param y - The second argument if invoked in immediate (2 arg) mode, or a
 *            macro to evaluate against args if invoked in lazy (3 arg) mode.
 * @param a - The argument list for the macro supplied in y (parenthetically
 *            encapsulated), or blank.
 *
 * @retval 1 Iff the logical connective evaluated to true.
 * @retval 0 Iff the logical connective evaluated to false.
 * 
 * @see ISTRUE() Determines whether or not each argument is considered true or
 *               false.
 */
/**
 * @ingroup cpp_logical_ops
 *
 * Logical or.
 */
#define OR(x, ...) PASTE(OR, ISTRUE(x))(__VA_ARGS__,,~)
#define OR1(...) 1
#define OR0(...) PASTE(OR0, LOGIC_EVALM(__VA_ARGS__))
#define OR01 1
#define OR00 0

/**
 * @ingroup cpp_logical_ops
 *
 * Logical and.
 */
#define AND(x, ...) PASTE(AND, ISTRUE(x))(__VA_ARGS__,,~)
#define AND0(...) 0
#define AND1(...) PASTE(AND1, LOGIC_EVALM(__VA_ARGS__))
#define AND11 1
#define AND10 0

/**
 * @ingroup cpp_logical_ops
 *
 * Logical implication.
 */
#define IMPLIES(x, ...) PASTE(IMPLIES, ISTRUE(x))(__VA_ARGS__,,~)
#define IMPLIES0(...) 1
#define IMPLIES1(...) PASTE(IMPLIES1, LOGIC_EVALM(__VA_ARGS__))
#define IMPLIES11 1
#define IMPLIES10 0

/**
 * @ingroup cpp_logical_ops
 *
 * Logical not.
 */
#define NOT(x) PASTE(NOT, ISTRUE(x))
#define NOT0 1
#define NOT1 0

/**
 * Determines whether or not the supplied expression is considered true.
 *
 * @param x - Any non-list expression. A blank (non-token) '', '0' or '()' are
 *            all considered false. Anything else is considered true.
 *
 * @param 0 Iff x was false.
 * @param 1 Iff x was true.
 */
#define ISTRUE(x) PASTE(ISTRUE_B, ISBLANK(x))(x)
#define ISTRUE_B1(x) 0
#define ISTRUE_B0(x) PASTE(ISTRUE_Z, ISZERO(x))(x)
#define ISTRUE_Z1(x) 0
#define ISTRUE_Z0(x) PASTE(ISTRUE_E, ISEMPTY(x))(x)
#define ISTRUE_E1(x) 0
#define ISTRUE_E0(x) 1

/**
 * Immediate if. Forms the basis of short circuit logical evaluation. When
 * evaluated against the supplied condition x, this macro will expand to a
 * second function-like macro accepting two arguments. If x evaluates to
 * true, the function-like macro expanded to will select its first argument.
 * If x evaluates to false, then the macro expanded to will select its second
 * argument. This is usefull for performing short circuit evaluation, since
 * only the selected path will be evaluated.
 * 
 * @code
 * IIF(0)(FOO, BAR)(a, b); // -> BAR(a, b)
 * IIF(1)(FOO, BAR)(a, b); // -> FOO(a, b)
 * @endcode
 *
 * @param x - Any non-list expression. This argument will be evaluated with
 *            ISTRUE()
 *
 * @retval IIF0 iff x was false.
 * @retval IIF1 iff x was true.
 *
 * @see ISTRUE() The predicate that is used to determine whether or not x is
 *               considered true.
 */
#define IIF(x) PASTE(IIF, ISTRUE(x))
#define IIF0(t, f) f
#define IIF1(t, f) t


#endif /* CPP_LOGIC_H */

