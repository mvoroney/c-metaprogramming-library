/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 * 
 * This file contains various macros useful for manipulating numbers as lists
 * of digits, and to perform basic 10's complement operations. 10's complement
 * is the base-10 analog of 2's complement, and provides us with a base-10
 * representation of negative numbers.
 *
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_INTEGER_DIGITS_H
#define CPP_INTEGER_DIGITS_H

#include "cpp/util.h"
#include "cpp/list/util.h"
#include "cpp/logic.h"
#include "cpp/integer.h"
#include "cpp/arithmetic/digits/compare.h"
#include "cpp/integer/digits3.h"

/**
 * Converts a 10's complement number to it's final output form.
 */
#define INTEGER_SIGN(n) PASTE(INTEGER_SIGN, INTEGER_ISNEGATIVE(n))(n)
#define INTEGER_SIGN0(n) n
#define INTEGER_SIGN1(n) -INTEGER_NEGATE(n)

/**
 * Negates a base10 number in 10's complement notation
 */
#define INTEGER_NEGATE(a) INTEGER_NEGATE_EXPAND(DEFER(1, DIGITS_INTEGER)(DIGITS_COMPL(INTEGER_DIGITS(a))))
#define INTEGER_NEGATE_EXPAND(...) __VA_ARGS__

#define DIGITS_COMPL(...) DIGITS_COMPL_EXPAND1(DEFER(1, DIGITS_COMPL_I)(__VA_ARGS__))
#define DIGITS_COMPL_I(...) DIGITS_COMPL_II(LIST_LENGTH(__VA_ARGS__), __VA_ARGS__)
#define DIGITS_COMPL_II(n, ...) DIGITS_COMPL_EXPAND0(DEFER(1, DIGITS_COMPL_R)(ADD_DIGITS(0, PASTE(DIGITS_COMPL_SUM, n)(__VA_ARGS__))))
#define DIGITS_COMPL_R(c, ...) __VA_ARGS__
#define DIGITS_COMPL_EXPAND0(...) __VA_ARGS__
#define DIGITS_COMPL_EXPAND1(...) __VA_ARGS__

#define DIGITS_COMPL2(d1, d2) DIGIT_COMPL(d1),DIGIT_COMPL(d2)
#define DIGITS_COMPL3(d1, d2, d3) DIGIT_COMPL(d1),DIGIT_COMPL(d2),DIGIT_COMPL(d3)
#define DIGITS_COMPL4(d1, d2, d3, d4) DIGITS_COMPL2(d1, d2),DIGITS_COMPL2(d3, d4)
#define DIGITS_COMPL5(d1, d2, d3, d4, d5) DIGITS_COMPL2(d1, d2),DIGITS_COMPL3(d3, d4, d5)
#define DIGITS_COMPL6(d1, d2, d3, d4, d5, d6) DIGITS_COMPL3(d1, d2, d3),DIGITS_COMPL3(d4, d5, d6)

#define DIGITS_COMPL_SUM3(...) DIGITS_COMPL3(__VA_ARGS__), 0,0,1
#define DIGITS_COMPL_SUM6(...) DIGITS_COMPL6(__VA_ARGS__), 0,0,0,0,0,1

#define DIGIT_COMPL(d) PASTE(DIGIT_COMPL, d)

#define DIGIT_COMPL0 9
#define DIGIT_COMPL1 8
#define DIGIT_COMPL2 7
#define DIGIT_COMPL3 6
#define DIGIT_COMPL4 5
#define DIGIT_COMPL5 4
#define DIGIT_COMPL6 3
#define DIGIT_COMPL7 2
#define DIGIT_COMPL8 1
#define DIGIT_COMPL9 0

/**
 * Determines whether or not the given 10's complement number is negative
 */
#define INTEGER_ISNEGATIVE(n) INTEGER_ISNEGATIVE_I((INTEGER_DIGITS(n), ~, ~))
#define INTEGER_ISNEGATIVE_I(digits) DIGITS_ISNEGATIVE digits

/**
 * Examines the leading digit of a 10's complement number to determine whether
 * or not it should be considered as negative
 */
#define DIGITS_ISNEGATIVE(...) DIGITS_ISNEGATIVE_(__VA_ARGS__, ~)
#define DIGITS_ISNEGATIVE_(d, ...) PASTE(DIGIT_ISNEGATIVE, d)

/**
 * @private
 * @{
 */
#define DIGIT_ISNEGATIVE0 0
#define DIGIT_ISNEGATIVE1 0
#define DIGIT_ISNEGATIVE2 0
#define DIGIT_ISNEGATIVE3 0
#define DIGIT_ISNEGATIVE4 0
#define DIGIT_ISNEGATIVE5 1
#define DIGIT_ISNEGATIVE6 1
#define DIGIT_ISNEGATIVE7 1
#define DIGIT_ISNEGATIVE8 1
#define DIGIT_ISNEGATIVE9 1
/** @} */

/**
 * Converts a digit list to the corresponding number. Handles up to 6 digit
 * numbers on input. Uses a minimum width of 3 for negative sign exteded
 * numbers.
 */
#define DIGITS_INTEGER(...) DIGITS_INTEGERW(3, __VA_ARGS__) 

/**
 * Converts the supplied list of digits into the corresponding integer representation.
 *
 * @param w   - The minimum width of the integer with sign extension. For
 *              positive numbers this parameter will be ignored and 1 will be
 *              used instead. For negative numbers w must be > 0
 * @param ... - A list of digits to be converted to a single token integer
 *              representation.
 */
#define DIGITS_INTEGERW(...) DIGITS_INTEGER_EXPANDW0(DEFER(1, DIGITS_INTEGERW_I)(__VA_ARGS__))
#define DIGITS_INTEGERW_I(w, ...) PASTE(DIGITS_INTEGERW_I, DIGITS_ISNEGATIVE(__VA_ARGS__))(LIST_LENGTH(__VA_ARGS__), w, __VA_ARGS__)
#define DIGITS_INTEGERW_I0(n, w, ...) DIGITS_INTEGERW_II(n, 1, 0, __VA_ARGS__) 
#define DIGITS_INTEGERW_I1(n, w, ...) DIGITS_INTEGERW_II(n, w, 9, __VA_ARGS__)
#define DIGITS_INTEGERW_II(n, w, z, ...) DIGITS_INTEGER_EXPANDW1(DIGITS_INTEGER_EXPAND0(DEFER(1, DIGITS_INTEGER_R)(PREVN(w, n), z, __VA_ARGS__)))

#define DIGITS_INTEGER_EXPANDW0(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPANDW1(...) __VA_ARGS__

// iterative digit reduction
#define DIGITS_INTEGER_R(n, z, ...) PASTE(DIGITS_INTEGER_NEXT, AND(n, DIGIT_EQ(FIRST(__VA_ARGS__), z)))(n, z,  __VA_ARGS__)

#define DIGITS_INTEGER_INDIRECT() DIGITS_INTEGER_R

#define DIGITS_INTEGER_NEXT0(n, z, ...) PASTEN(LIST_LENGTH(__VA_ARGS__), __VA_ARGS__)
#define DIGITS_INTEGER_NEXT1(n, z, d1, ...) DEFER(1, PASTE(DIGITS_INTEGER_EXPAND, n))(DEFER(1, DIGITS_INTEGER_INDIRECT)()(PREV(n), z, __VA_ARGS__))

/**
 * @private
 * macro expanders
 * @{
 */
#define DIGITS_INTEGER_EXPAND0(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND1(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND2(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND3(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND4(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND5(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND6(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND7(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND8(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND9(...) __VA_ARGS__
#define DIGITS_INTEGER_EXPAND10(...) __VA_ARGS__
/** @} */

/**
 * Converts the supplied number to a digit-list.
 */
#define INTEGER_DIGITS(n) INTEGER_DIGITS3(n) 

#endif /* CPP_INTEGER_DIGITS_H */

