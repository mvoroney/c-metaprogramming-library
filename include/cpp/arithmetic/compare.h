/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * This file contains integer comparison macros
 *
 * @author Matt Voroney matt.voroney@gmail.com
 */
#pragma once
#ifndef CPP_INTEGER_COMPARE_H
#define CPP_INTEGER_COMPARE_H

#include "cpp/util.h"
#include "cpp/logic.h"
#include "cpp/arithmetic/adder.h"

/**
 * Compares two integers to see if they are equal.
 *
 * @param a - The first value to compare.
 * @param b - The second value to compare.
 *
 * @retval 1 if the two integers were equal.
 * @retval 0 if the two integers were not equal.
 */
#define INTEGER_EQ(a, b) ISZERO(SUB(a, b)) 

/**
 * Compares two integers to see if the first is less than the second.
 *
 * @param a - The first value to compare.
 * @param b - The second value to compare.
 *
 * @retval 1 if a < b.
 * @retval 0 if a !< b.
 */
#define INTEGER_LT(a, b) INTEGER_ISNEGATIVE(SUB(a, b))

/**
 * Compares two integers to see if the first is greater than the second.
 *
 * @param a - The first value to compare.
 * @param b - The second value to compare.
 *
 * @retval 1 if a > b.
 * @retval 0 if a !> b.
 */
#define INTEGER_GT(a, b) INTEGER_ISNEGATIVE(SUB(b, a))

/**
 * Compares two integers to see if the first is less than or equal to the
 * second.
 *
 * @param a - The first value to compare.
 * @param b - The second value to compare.
 *
 * @retval 1 if a <= b.
 * @retval 0 if a !<= b.
 */
#define INTEGER_LE(a, b) OR(INTEGER_EQ(a, b), INTEGER_LT(a, b))

/**
 * Compares two integers to see if the first is greater than or equal to the
 * second.
 *
 * @param a - The first value to compare.
 * @param b - The second value to compare.
 *
 * @retval 1 if a >= b.
 * @retval 0 if a !>= b.
 */
#define INTEGER_GE(a, b) OR(INTEGER_EQ(a, b), INTEGER_GT(a, b))

#endif /* CPP_INTEGER_COMPARE_H */

