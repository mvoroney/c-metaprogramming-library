/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * This file contains macros for performing addition, subtraction and absolute
 * value on 10's complement number representations.
 *
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_ARITHMETIC_ADDER_H
#define CPP_ARITHMETIC_ADDER_H

#include "cpp/util.h"
#include "cpp/list/util.h"
#include "cpp/arithmetic/digits.h"
#include "cpp/arithmetic/digits/compare.h"

#define ABS(x) PASTE(ABS, INTEGER_ISNEGATIVE(x))(x)
#define ABS0(x) x
#define ABS1(x) INTEGER_NEGATE(x)

#define ADD(x, y) OVERFLOW_CHECK(ADD, x, y, ADD_DIGITS(0, INTEGER_DIGITS(x), INTEGER_DIGITS(y)))

#define OVERFLOW_CHECK_EXPAND(...) __VA_ARGS__
#define OVERFLOW_CHECK(...) OVERFLOW_CHECK_EXPAND(DEFER(1, OVERFLOW_CHECK_I)(__VA_ARGS__))
#define OVERFLOW_CHECK_I(op, ...) PASTE3(OVERFLOW_CHECK_, op, LIST_LENGTH(__VA_ARGS__))(__VA_ARGS__)

#define OVERFLOW_CHECK_ADD3(x, y, c1) c1
#define OVERFLOW_CHECK_ADD5(x, y, c1, d1, d2) DIGITS_INTEGER(d1, d2)
#define OVERFLOW_CHECK_ADD6(x, y, c1, d1, d2, d3) DIGITS_INTEGER(d1, d2, d3)
#define OVERFLOW_CHECK_ADD9(x, y, c1, d1, d2, d3, d4, d5, d6) DIGITS_INTEGER(d1, d2, d3, d4, d5, d6)

#define SUB(x, y) OVERFLOW_CHECK(SUB, x, y, ADD_DIGITS(0, INTEGER_DIGITS(x), DIGITS_COMPL(INTEGER_DIGITS(y))))

#define OVERFLOW_CHECK_SUB6(x, y, c1, d1, d2, d3) DIGITS_INTEGER(d1, d2, d3)

/**
 * 3 digit Ripple cary adder. Does not check for overflow.
 */
#define ADD_DIGITS(...) ADD_DIGITS_EXPAND0(DEFER(1, ADD_DIGITS_I)(__VA_ARGS__))
#define ADD_DIGITS_I(...) PASTE(ADD_DIGITS, LIST_LENGTH(__VA_ARGS__))(__VA_ARGS__)
#define ADD_DIGITS5(...) ADD_DIGITS2_0(__VA_ARGS__)
#define ADD_DIGITS7(...) ADD_DIGITS3_0(__VA_ARGS__)
#define ADD_DIGITS13(...) ADD_DIGITS6_0(__VA_ARGS__)

/**
 * @private
 * @note use expanders by order of prime multiples in sequence. This should
 * reduce the number of expanders required overall while guaranteeing no colisions.
 */
#define ADD_DIGITS2_0(c, a1, a2, b1, b2) ADD_DIGITS_EXPAND1(DEFER(1, ADD_DIGITS2_1)(a1, b1, ADD_DIGITC(c, a2, b2)))
#define ADD_DIGITS2_1(a1, b1, c1, d1) ADD_DIGITC(c1, a1, b1),d1
#define ADD_DIGITS3_0(c, a1, a2, a3, b1, b2, b3) ADD_DIGITS_EXPAND1(DEFER(1, ADD_DIGITS3_1)(a1, b1, ADD_DIGITS2_0(c, a2, a3, b2, b3)))
#define ADD_DIGITS3_1(a1, b1, c1, d1, d2) ADD_DIGITC(c1, a1, b1),d1,d2
#define ADD_DIGITS4_0(c, a1, a2, a3, a4, b1, b2, b3, b4) ADD_DIGITS_EXPAND2(DEFER(1, ADD_DIGITS4_1)(a1, a2, b1, b2, ADD_DIGITS2_0(c, a3, a4, b3, b4)))
#define ADD_DIGITS4_1(a1, a2, b1, b2, c1, d1, d2) ADD_DIGITS2_0(c1, a1, a2, b1, b2),d1,d2
#define ADD_DIGITS5_0(c, a1, a2, a3, a4, a5, b1, b2, b3, b4, b5) ADD_DIGITS_EXPAND2(DEFER(1, ADD_DIGITS5_1)(a1, a2, b1, b2, ADD_DIGITS3_0(c, a3, a4, a5, b3, b4, b5)))
#define ADD_DIGITS5_1(a1, a2, b1, b2, c2, d1, d2, d3) ADD_DIGITS2_0(c2, a1, a2, b1, b2),d1,d2,d3
#define ADD_DIGITS6_0(c, a1, a2, a3, a4, a5, a6, b1, b2, b3, b4, b5, b6) ADD_DIGITS_EXPAND2(DEFER(1, ADD_DIGITS6_1)(a1, a2, a3, b1, b2, b3, ADD_DIGITS3_0(c, a4, a5, a6, b4, b5, b6)))
#define ADD_DIGITS6_1(a1, a2, a3, b1, b2, b3, c3, d1, d2, d3) ADD_DIGITS3_0(c3, a1, a2, a3, b1, b2, b3),d1,d2,d3

#define ADD_DIGITS_EXPAND0(...) __VA_ARGS__
#define ADD_DIGITS_EXPAND1(...) __VA_ARGS__
#define ADD_DIGITS_EXPAND2(...) __VA_ARGS__
#define ADD_DIGITS_EXPAND3(...) __VA_ARGS__

/**
 * full adder.
 */
#define ADD_DIGITC(c, a, b) ADD_DIGITC_EXPAND1(DEFER(1, PASTE(ADD_DIGITC, c))(ADD_DIGIT(a, b)))
#define ADD_DIGITC0(...) __VA_ARGS__
#define ADD_DIGITC1(a, b) ADD_DIGITC_EXPAND2(DEFER(1, PASTE(ADD_DIGITCI, a))(ADD_DIGIT(1, b)))
#define ADD_DIGITCI0(...) __VA_ARGS__
#define ADD_DIGITCI1(a, b) 1,b

#define ADD_DIGITC_EXPAND1(...) __VA_ARGS__
#define ADD_DIGITC_EXPAND2(...) __VA_ARGS__

#define ADD_DIGIT(x, y) PASTE(ADD_DIGIT, DIGIT_GT(x, y))(x, y)
#define ADD_DIGIT0(x, y) PASTE(ADD_DIGITZ, ISZERO(x))(x, y)
#define ADD_DIGIT1(x, y) PASTE(ADD_DIGITZ, ISZERO(y))(y, x)
#define ADD_DIGITZ0(x, y) PASTE4(ADD_DIGIT_, x, _, y)
#define ADD_DIGITZ1(x, y) 0,y

/**
 * @private
 * @{
 * Implementation of a 10 * 10 addition table.
 * 
 * @note Only the upper half of the table needs to be implemented, since
 * addition is commutative. 
 * 
 * @verbatim
 * +  1  2  3  4  5  6  7  8  9
 * 1  2  3  4  5  6  7  8  9 10
 * 2     4  5  6  7  8  9 10 11
 * 3        6  7  8  9 10 11 12
 * 4           8  9 10 11 12 13 
 * 5             25 30 35 40 45
 * 6                36 42 48 54
 * 7                   49 56 63
 * 8                      64 72
 * 9                         81
 * @endverbatim
 */
#define ADD_DIGIT_1_1 0,2
#define ADD_DIGIT_1_2 0,3
#define ADD_DIGIT_1_3 0,4
#define ADD_DIGIT_1_4 0,5
#define ADD_DIGIT_1_5 0,6
#define ADD_DIGIT_1_6 0,7
#define ADD_DIGIT_1_7 0,8
#define ADD_DIGIT_1_8 0,9
#define ADD_DIGIT_1_9 1,0
#define ADD_DIGIT_2_2 0,4
#define ADD_DIGIT_2_3 0,5
#define ADD_DIGIT_2_4 0,6
#define ADD_DIGIT_2_5 0,7
#define ADD_DIGIT_2_6 0,8
#define ADD_DIGIT_2_7 0,9
#define ADD_DIGIT_2_8 1,0
#define ADD_DIGIT_2_9 1,1
#define ADD_DIGIT_3_3 0,6
#define ADD_DIGIT_3_4 0,7
#define ADD_DIGIT_3_5 0,8
#define ADD_DIGIT_3_6 0,9
#define ADD_DIGIT_3_7 1,0
#define ADD_DIGIT_3_8 1,1
#define ADD_DIGIT_3_9 1,2
#define ADD_DIGIT_4_4 0,8
#define ADD_DIGIT_4_5 0,9
#define ADD_DIGIT_4_6 1,0
#define ADD_DIGIT_4_7 1,1
#define ADD_DIGIT_4_8 1,2
#define ADD_DIGIT_4_9 1,3
#define ADD_DIGIT_5_5 1,0
#define ADD_DIGIT_5_6 1,1
#define ADD_DIGIT_5_7 1,2
#define ADD_DIGIT_5_8 1,3
#define ADD_DIGIT_5_9 1,4
#define ADD_DIGIT_6_6 1,2
#define ADD_DIGIT_6_7 1,3
#define ADD_DIGIT_6_8 1,4
#define ADD_DIGIT_6_9 1,5
#define ADD_DIGIT_7_7 1,4
#define ADD_DIGIT_7_8 1,5
#define ADD_DIGIT_7_9 1,6
#define ADD_DIGIT_8_8 1,6
#define ADD_DIGIT_8_9 1,7
#define ADD_DIGIT_9_9 1,8
/** @} */

#endif /* CPP_ARITHMETIC_ADDER_H */

