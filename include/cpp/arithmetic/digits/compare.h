/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * Contains various comparison macros for digits. This file is for internal
 * use, or for extending core capabilities.
 *
 * @note public macros should be safe with near arbitrary input. Non-digits
 * should be safely ignored.
 * 
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_ARITHMETIC_DIGITS_COMPARE_H
#define CPP_ARITHMETIC_DIGITS_COMPARE_H

#include "cpp/util.h"
#include "cpp/logic.h"

/**
 * @private
 * @{
 */
#define DIGIT_LT_EXPAND(...) __VA_ARGS__
#define DIGIT_GT_EXPAND(...) __VA_ARGS__
#define DIGIT_EQ_EXPAND(...) __VA_ARGS__
/** @} */

/**
 * Determines whether x is greater than y.
 *
 * @param x - The first digit to compare
 * @param y - The second digit to compare
 *
 * @retval 1 - Iff x was greater than y.
 * @retval 0 - If x was not greater than y, or either x or y was not a digit. 
 */
#define DIGIT_GT(x, y) OR(DIGIT_GT_(x, y), DIGIT_LT_(y, x))

/**
 * Determines whether x is less than y.
 *
 * @param x - The first digit to compare
 * @param y - The second digit to compare
 *
 * @retval 1 - Iff x was less than y.
 * @retval 0 - If x was not less than y, or either x or y was not a digit. 
 */
#define DIGIT_LT(x, y) OR(DIGIT_LT_(x, y), DIGIT_GT_(y, x))

/**
 * Determines whether x is greater than or equal to y.
 *
 * @param x - The first digit to compare
 * @param y - The second digit to compare
 *
 * @retval 1 - Iff x is greater than or equal to y.
 * @retval 0 - If x is less than y, or either x or y was not a digit. 
 */
#define DIGIT_GE(x, y) OR(DIGIT_GT(x, y), DIGIT_EQ(x, y))

/**
 * Determines whether x is less than or equal to y.
 *
 * @param x - The first digit to compare
 * @param y - The second digit to compare
 *
 * @retval 1 - Iff x is less than or equal to y.
 * @retval 0 - If x was greater than y, or either x or y was not a digit. 
 */
#define DIGIT_LE(x, y) OR(DIGIT_LT(x, y), DIGIT_EQ(x, y))

/**
 * Determines whether two digits are equal.
 *
 * @param x - The first digit to compare
 * @param y - The second digit to compare
 *
 * @retval 1 - Iff x and y were the same digit.
 * @retval 0 - If x and y were not the same, or either x or y was not a digit. 
 */
#define DIGIT_EQ(x, y) DIGIT_EQ_EXPAND(DEFER(1, SECOND)(PASTE3(DIGIT_EQ_, x, y), 0))

/**
 * @private
 * @{
 *
 * Implementations of equality, greater than and less than comparison for
 * single digit numbers.
 *
 * @note the greater than and less than tables are designed to be complementary,
 * so neither table is complete by itself. This allows us to take advantage of
 * the fact that large numbers have few numbers greater than them, and small
 * numbers have few numbers less than them. Each digit will have at most at
 * most 5 table entries, and 2.5 on average.
 */
#define DIGIT_EQ_00 ~,1
#define DIGIT_EQ_11 ~,1
#define DIGIT_EQ_22 ~,1
#define DIGIT_EQ_33 ~,1
#define DIGIT_EQ_44 ~,1
#define DIGIT_EQ_55 ~,1
#define DIGIT_EQ_66 ~,1
#define DIGIT_EQ_77 ~,1
#define DIGIT_EQ_88 ~,1
#define DIGIT_EQ_99 ~,1

#define DIGIT_GT_(x, y) DIGIT_GT_EXPAND(DEFER(1, SECOND)(PASTE3(DIGIT_GT_, x, y), 0))

#define DIGIT_GT_10 ~,1
#define DIGIT_GT_21 ~,1
#define DIGIT_GT_20 ~,1
#define DIGIT_GT_32 ~,1
#define DIGIT_GT_31 ~,1
#define DIGIT_GT_30 ~,1
#define DIGIT_GT_43 ~,1
#define DIGIT_GT_42 ~,1
#define DIGIT_GT_41 ~,1
#define DIGIT_GT_40 ~,1
#define DIGIT_GT_54 ~,1
#define DIGIT_GT_53 ~,1
#define DIGIT_GT_52 ~,1
#define DIGIT_GT_51 ~,1
#define DIGIT_GT_50 ~,1
#define DIGIT_GT_63 ~,1
#define DIGIT_GT_62 ~,1
#define DIGIT_GT_61 ~,1
#define DIGIT_GT_60 ~,1
#define DIGIT_GT_71 ~,1
#define DIGIT_GT_70 ~,1
#define DIGIT_GT_80 ~,1

#define DIGIT_LT_(x, y) DIGIT_LT_EXPAND(DEFER(1, SECOND)(PASTE3(DIGIT_LT_, x, y), 0))

#define DIGIT_LT_09 ~,1
#define DIGIT_LT_18 ~,1
#define DIGIT_LT_19 ~,1
#define DIGIT_LT_27 ~,1
#define DIGIT_LT_28 ~,1
#define DIGIT_LT_29 ~,1
#define DIGIT_LT_37 ~,1
#define DIGIT_LT_38 ~,1
#define DIGIT_LT_39 ~,1
#define DIGIT_LT_46 ~,1
#define DIGIT_LT_47 ~,1
#define DIGIT_LT_48 ~,1
#define DIGIT_LT_49 ~,1
#define DIGIT_LT_56 ~,1
#define DIGIT_LT_57 ~,1
#define DIGIT_LT_58 ~,1
#define DIGIT_LT_59 ~,1
#define DIGIT_LT_67 ~,1
#define DIGIT_LT_68 ~,1
#define DIGIT_LT_69 ~,1
#define DIGIT_LT_78 ~,1
#define DIGIT_LT_79 ~,1
#define DIGIT_LT_89 ~,1
/** @} */


#endif /* CPP_ARITHMETIC_DIGITS_COMPARE_H */

