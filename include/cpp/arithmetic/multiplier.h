/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_ARITHMETIC_MULTIPLIER_H
#define CPP_ARITHMETIC_MULTIPLIER_H

#include "cpp/util.h"
#include "cpp/logic.h"
#include "cpp/list/util.h"
#include "cpp/arithmetic/digits.h"
#include "cpp/arithmetic/digits/compare.h"
#include "cpp/arithmetic/adder.h"

#define MULT(x, y) MULT_I(INTEGER_ISNEGATIVE(x), INTEGER_ISNEGATIVE(y), x, y)
#define MULT_I(a, b, x, y) OVERFLOW_CHECK(MULT, x, y, MULT_II(a, b, x, y))
#define MULT_II(a, b, x, y) PASTE3(MULT, a, b)(x, y)

#define MULT00(x, y) MUL_DIGITS(0, INTEGER_DIGITS(x), INTEGER_DIGITS(y))
#define MULT01(x, y) MULT_EXPAND(DEFER(1, MULT_COMPL)(MUL_DIGITS(0, INTEGER_DIGITS(x), DIGITS_COMPL(INTEGER_DIGITS(y)))))
#define MULT10(x, y) MULT_EXPAND(DEFER(1, MULT_COMPL)(MUL_DIGITS(0, DIGITS_COMPL(INTEGER_DIGITS(x)), INTEGER_DIGITS(y))))
#define MULT11(x, y) MUL_DIGITS(0, DIGITS_COMPL(INTEGER_DIGITS(x)), DIGITS_COMPL(INTEGER_DIGITS(y)))

// @note cary should always be 0, since any 3 digit multiplication should fit in 6 digits.
#define MULT_COMPL(c, ...) DIGITS_COMPL(__VA_ARGS__)
#define MULT_EXPAND(...) __VA_ARGS__

#define OVERFLOW_CHECK_MULT4(x, y, c, d1) DIGITS_INTEGER(0, 0, d1)
#define OVERFLOW_CHECK_MULT5(x, y, c, d1, d2) DIGITS_INTEGER(0, d1, d2)
#define OVERFLOW_CHECK_MULT6(x, y, c, d1, d2, d3) DIGITS_INTEGER(d1, d2, d3)
#define OVERFLOW_CHECK_MULT7(x, y, c, d1, d2, d3, d4) DIGITS_INTEGER(d1, d2, d3, d4)
#define OVERFLOW_CHECK_MULT8(x, y, c, d1, d2, d3, d4, d5) DIGITS_INTEGER(d1, d2, d3, d4, d5)
#define OVERFLOW_CHECK_MULT9(x, y, c, d1, d2, d3, d4, d5, d6) DIGITS_INTEGER(d1, d2, d3, d4, d5, d6)

#define MUL_DIGITS_EXPAND0(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND1(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND2(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND3(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND4(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND5(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND6(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND7(...) __VA_ARGS__
#define MUL_DIGITS_EXPAND8(...) __VA_ARGS__

#define MUL_DIGITS(...) MUL_DIGITS_EXPAND0(DEFER(1, MUL_DIGITS_I)(__VA_ARGS__))
#define MUL_DIGITS_I(...) PASTE(MUL_DIGITS, LIST_LENGTH(__VA_ARGS__))(__VA_ARGS__)
#define MUL_DIGITS5(...) MUL_DIGITS2_0(__VA_ARGS__)
#define MUL_DIGITS7(...) MUL_DIGITS3_0(__VA_ARGS__)

#define MUL_DIGITS2_0(c, a1, a2, b1, b2) MUL_DIGITS_EXPAND1(DEFER(1, MUL_DIGITS2_1)(c, MUL_DIGIT(a1, b1), MUL_DIGIT(a1, b2), MUL_DIGIT(a2, b1), MUL_DIGIT(a2, b2)))
#define MUL_DIGITS2_1(c, c1, d1, c2, d2, c3, d3, c4, d4) MUL_DIGITS_EXPAND2(DEFER(1, MUL_DIGITS2_2)(ADD_DIGITS4_0(0, c1,d1,0,0, 0,c2,d2,0), ADD_DIGITS4_0(0, 0,c3,d3,c, 0,0,c4,d4)))
#define MUL_DIGITS2_2(c1, a1, a2, a3, a4, c2, b1, b2, b3, b4) ADD_DIGITS4_0(0, a1,a2,a3,a4, b1,b2,b3,b4)

#define MUL_DIGITS3_0(c, a1, a2, a3, b1, b2, b3) MUL_DIGITS_EXPAND4(DEFER(1, MUL_DIGITS3_1)(c, MUL_DIGIT(a1,b1), MUL_DIGITS2_0(0, a2,a3, 0,b1), MUL_DIGITS2_0(0, 0,a1, b2,b3), MUL_DIGITS2_0(0, a2,a3, b2,b3)))
#define MUL_DIGITS3_1(c, c1,a1, c2,b1,b2,b3,b4, c3,d1,d2,d3,d4, c4,e1,e2,e3,e4) MUL_DIGITS_EXPAND5(DEFER(1, MUL_DIGITS3_2)(ADD_DIGITS6_0(0, c1,a1,0,0,0,0, b1,b2,b3,b4,0,0), ADD_DIGITS6_0(0, d1,d2,d3,d4,0,c, 0,0,e1,e2,e3,e4)))
#define MUL_DIGITS3_2(c1,a1,a2,a3,a4,a5,a6, c2,b1,b2,b3,b4,b5,b6) ADD_DIGITS6_0(0, a1,a2,a3,a4,a5,a6, b1,b2,b3,b4,b5,b6)

#define MUL_DIGIT(x, y) PASTE(MUL_DIGIT, DIGIT_GT(x, y))(x, y)

#define MUL_DIGIT0(x, y) PASTE(MUL_DIGITZ, DIGIT_EQ(0, x))(x, y)
#define MUL_DIGIT1(x, y) PASTE(MUL_DIGITZ, DIGIT_EQ(0, y))(y, x)

#define MUL_DIGITZ0(x, y) PASTE(MUL_DIGITQ, DIGIT_EQ(1, x))(x, y)
#define MUL_DIGITZ1(x, y) 0,0

#define MUL_DIGITQ0(x, y) PASTE4(MUL_DIGIT_, x, _, y)
#define MUL_DIGITQ1(x, y) 0,y

/**
 * @private
 * @{
 * Implementation of upper diagonal of a 10 * 10 multiplication table. i.e.
 * 
 * @verbatim
 * *  2  3  4  5  6  7  8  9
 * 1  2  3  4  5  6  7  8  9
 * 2  4  6  8 10 12 14 16 18
 * 3     9 12 15 18 21 24 27
 * 4       16 20 24 28 32 36
 * 5          25 30 35 40 45
 * 6             36 42 48 54
 * 7                49 56 63
 * 8                   64 72
 * 9                      81
 * @endverbatim
 */
#define MUL_DIGIT_2_2 0,4
#define MUL_DIGIT_2_3 0,6
#define MUL_DIGIT_2_4 0,8
#define MUL_DIGIT_2_5 1,0
#define MUL_DIGIT_2_6 1,2
#define MUL_DIGIT_2_7 1,4
#define MUL_DIGIT_2_8 1,6
#define MUL_DIGIT_2_9 1,8
#define MUL_DIGIT_3_3 0,9
#define MUL_DIGIT_3_4 1,2
#define MUL_DIGIT_3_5 1,5
#define MUL_DIGIT_3_6 1,8
#define MUL_DIGIT_3_7 2,1
#define MUL_DIGIT_3_8 2,4
#define MUL_DIGIT_3_9 2,7
#define MUL_DIGIT_4_4 1,6
#define MUL_DIGIT_4_5 2,0
#define MUL_DIGIT_4_6 2,4
#define MUL_DIGIT_4_7 2,8
#define MUL_DIGIT_4_8 3,2
#define MUL_DIGIT_4_9 3,6
#define MUL_DIGIT_5_5 2,5
#define MUL_DIGIT_5_6 3,0
#define MUL_DIGIT_5_7 3,5
#define MUL_DIGIT_5_8 4,0
#define MUL_DIGIT_5_9 4,5
#define MUL_DIGIT_6_6 3,6
#define MUL_DIGIT_6_7 4,2
#define MUL_DIGIT_6_8 4,8
#define MUL_DIGIT_6_9 5,4
#define MUL_DIGIT_7_7 4,9
#define MUL_DIGIT_7_8 5,6
#define MUL_DIGIT_7_9 6,3
#define MUL_DIGIT_8_8 6,4
#define MUL_DIGIT_8_9 7,2
#define MUL_DIGIT_9_9 8,1
/** @} */

#endif /* CPP_ARITHMETIC_MULTIPLIER_H */

