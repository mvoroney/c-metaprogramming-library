/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * This file contains macros implementing left and right fold list operators.
 * When using these operators to implement other iterative operations, define
 * NO_DEFAULT_EXPANDER to allow a different expander to be used instead. This
 * is adviseable in cases where it is undesireable to expose the iterative
 * nature of the macro, since this is the only way to avoid colisions if used
 * in conjunction with other iterative macros.
 *
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_LIST_FOLD_H
#define CPP_LIST_FOLD_H

#include "cpp/util.h"
#include "cpp/logic.h"
#include "cpp/integer.h"
#include "cpp/list/util.h"
#include "cpp/placeholder.h"

#ifndef NO_DEFAULT_EXPANDER
#   include "cpp/expand.h"
#endif

/**
 * @private
 *
 * List fold generic back end
 *
 * @{
 */
#define LIST_FOLDL_I(expand, level, direction, macro, args, ...) expand(level)(0)(LIST_FOLD_I(expand, level, PREV(LIST_LENGTH(__VA_ARGS__)), direction, macro, LIST_LENGTH args, args, __VA_ARGS__, ~))

#define LIST_FOLD_ACCUMULATOR(expand, level, state, direction, macro, nargs, args, accumulator, ...) accumulator
#define LIST_FOLD_VALS(direction, a1, a2, level) PASTE(LIST_FOLD_VALS_, direction)(a1, a2, level)

#define LIST_FOLD_I(expand, level, state, direction, macro, nargs, args, ...) IIF(state)(LIST_FOLD_NEXT, LIST_FOLD_ACCUMULATOR)(expand, level, state, direction, macro, nargs, args, __VA_ARGS__)

#define LIST_FOLD_INDIRECT() LIST_FOLD_I

#define LIST_FOLD_NEXT(expand, level, state, direction, macro, nargs, args, accumulator, a1, ...) DEFER(1, expand(level)(state))(DEFER(1, LIST_FOLD_INDIRECT)()(expand, level, PREV(state), direction, macro, nargs, args, PLACEHOLDER_EVALM(macro, nargs, LIST_FOLD_VALS(direction, a1, accumulator, NEXT(level)), args), __VA_ARGS__))
/** @} */

/**
 * Generic list fold operator allowing an arbitrary expander to be used.
 * All other fold variants are implemented in terms of this macro using the
 * default expander.
 * 
 * @ see EXPAND() for details on the default expander, and expander requirements.
 */
#define LIST_FOLDLE(expand, level, direction, macro, args, ...) LIST_FOLDL_I(expand, level, direction, macro, args, LIST_DECAPSULATE(__VA_ARGS__))

/**
 * Successively combines the elements of the list in left to right order
 * according to the supplied macro.
 *
 * @param macro - The macro to use to combine list elements.
 * @param args  - The argument list to be supplied to the macro. Placeholder
 *                expansion is peformed on the list prior to invocation of the
 *                macro.
 *                - _1 expands to the accumulated value.
 *                - _2 expands to the current list value.
 *                - _3 expands to the nesting level the macro was invoked at.
 * @param accum - A starting value for the accumulator. If the supplied
 *                list is empty, this will be returned as the default value.
 * 
 * @return The result of combining all of the elements in the list according
 *         to the supplied macro.
 */
#define LIST_RIGHT_FOLD(macro, args, ...) LIST_RIGHT_FOLDL(0, macro, args, __VA_ARGS__)
#define LIST_RIGHT_FOLD0(macro, args, ...) LIST_RIGHT_FOLDL(0, macro, args, __VA_ARGS__)
#define LIST_RIGHT_FOLD1(macro, args, ...) LIST_RIGHT_FOLDL(1, macro, args, __VA_ARGS__)
#define LIST_RIGHT_FOLD2(macro, args, ...) LIST_RIGHT_FOLDL(2, macro, args, __VA_ARGS__)
#define LIST_RIGHT_FOLD3(macro, args, ...) LIST_RIGHT_FOLDL(3, macro, args, __VA_ARGS__)
#define LIST_RIGHT_FOLD4(macro, args, ...) LIST_RIGHT_FOLDL(4, macro, args, __VA_ARGS__)
#define LIST_RIGHT_FOLD5(macro, args, ...) LIST_RIGHT_FOLDL(5, macro, args, __VA_ARGS__)

/**
 * Right fold with expander level 
 *
 * @{
 */
#define LIST_RIGHT_FOLDL(level, macro, args, ...) LIST_RIGHT_FOLDLE(EXPAND, level, macro, args, __VA_ARGS__)
#define LIST_RIGHT_FOLDLE(expand, level, macro, args, ...) LIST_FOLDLE(expand, level, RIGHT, macro, args, LIST_REVERSELE(expand, level, COMMA, __VA_ARGS__))

#define LIST_FOLD_VALS_RIGHT(a1, a2, level) (a1, a2, level, ~)
/** @} */

/**
 * Reverses the contents of the specified list.
 *
 * @todo move this to a more suitable file?
 */
#define LIST_REVERSEL(level, sep, ...) LIST_REVERSELE(EXPAND, level, sep, __VA_ARGS__)

/**
 * Reverse allowing an arbitrary expander to be used.
 */
#define LIST_REVERSELE(expand, level, sep, ...) LIST_REVERSE_EXPAND0(DEFER(1, LIST_REVERSE_I)(expand, level, sep, LIST_DECAPSULATE(__VA_ARGS__)))
#define LIST_REVERSE_I(expand, level, sep, ...) LIST_REVERSE_EXPAND1(DEFER(1, SECOND)(PASTE(LIST_REVERSE, LIST_LENGTH(__VA_ARGS__)), LIST_REVERSE_)(expand, level, sep, __VA_ARGS__))

#define LIST_REVERSE_(expand, level, sep, a1, ...) DECAPSULATE(LIST_FOLDL_I(expand, level, LEFT, LIST_REVERSE_OP, (sep, _2, _1), (a1), __VA_ARGS__))
#define LIST_REVERSE_0(expand, level, sep, ...) __VA_ARGS__
#define LIST_REVERSE_1(expand, level, sep, ...) __VA_ARGS__

#define LIST_REVERSE0 ~,LIST_REVERSE_0
#define LIST_REVERSE1 ~,LIST_REVERSE_1

#define LIST_REVERSE_EXPAND0(...) __VA_ARGS__
#define LIST_REVERSE_EXPAND1(...) __VA_ARGS__
#define LIST_REVERSE_OP(sep, a, b) (LIST_PASS(a)sep()DECAPSULATE(b))

/**
 * Left fold
 */
#define LIST_LEFT_FOLD(macro, args, ...) LIST_LEFT_FOLDL(0, macro, args, __VA_ARGS__)
#define LIST_LEFT_FOLD0(macro, args, ...) LIST_LEFT_FOLDL(0, macro, args, __VA_ARGS__)
#define LIST_LEFT_FOLD1(macro, args, ...) LIST_LEFT_FOLDL(1, macro, args, __VA_ARGS__)
#define LIST_LEFT_FOLD2(macro, args, ...) LIST_LEFT_FOLDL(2, macro, args, __VA_ARGS__)
#define LIST_LEFT_FOLD3(macro, args, ...) LIST_LEFT_FOLDL(3, macro, args, __VA_ARGS__)
#define LIST_LEFT_FOLD4(macro, args, ...) LIST_LEFT_FOLDL(4, macro, args, __VA_ARGS__)
#define LIST_LEFT_FOLD5(macro, args, ...) LIST_LEFT_FOLDL(5, macro, args, __VA_ARGS__)

/**
 * Left fold with expander level
 *
 * @{
 */
#define LIST_LEFT_FOLDL(level, macro, args, ...) LIST_LEFT_FOLDLE(EXPAND, level, macro, args, __VA_ARGS__)
#define LIST_LEFT_FOLDLE(expand, level, macro, args, ...) LIST_FOLDLE(expand, level, LEFT, macro, args, __VA_ARGS__)

#define LIST_FOLD_VALS_LEFT(a1, a2, level) (a2, a1, level, ~)
/** @} */

#endif /* CPP_LIST_FOLD_H */

