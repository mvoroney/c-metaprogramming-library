/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef CPP_LIST_UTIL_H
#define CPP_LIST_UTIL_H

#include "cpp/util.h"
#include "cpp/logic.h"

/**
 * Removes the outermost pair of parentheses from a list expression.
 *
 * @param list - Any sequence of 0 or more tokens.
 * @return The input token sequence with the outermost set of enclosing
 *         parentheses removed (if there were any).
 *
 * @see DECAPSULATE() LIST_DECAPSULATE() is a list-safe variant of
 *      DECAPSULATE()
 */
#define LIST_DECAPSULATE(...) PASTE(LIST_DECAPSULATE_, ISONE_E(LIST_LENGTH(__VA_ARGS__)))(__VA_ARGS__)
#define LIST_DECAPSULATE_1(x) DECAPSULATE(x)
#define LIST_DECAPSULATE_0(...) __VA_ARGS__

/**
 * Ensures that the supplied list is a parenthetical expression by adding
 * parentheses if necessary.
 *
 * @param list - Any sequence of 0 or more tokens.
 *
 * @retval list Iff the supplied list was already parenthetically encapsulated.
 * @retval (list) Iff the supplied list was not parenthetically encapsulated.
 *
 * @see LIST_ISPARENTHETICAL() For information on what is considered a
 *      parenthetical expression.
 * @see LIST_DECAPSULATE() The dual of this macro.
 */
#define LIST_ENCAPSULATE(...) PASTE(LIST_ENCAPSULATE_, ISONE_E(LIST_LENGTH(__VA_ARGS)))(__VA_ARGS__)
#define LIST_ENCAPSULATE_1(x) ENCAPSULATE(x)
#define LIST_ENCAPSULATE_0(...) (__VA_ARGS__) 

/**
 * Determines whether the list expression is encapsualted in parentheses or not.
 *
 * @param list - A list expression to test.
 *
 * @retval 1 if the list was parenthetically encapsulated.
 * @retval 0 otherwise.
 *
 * @see ISPARENTHETICAL() LIST_ISPARENTHETICAL() is a list-safe variant of
 *      ISPARENTHETICAL().
 */
#define LIST_ISPARENTHETICAL(...) PASTE(LIST_ISPARENTHETICAL_, ISONE_E(LIST_LENGTH(__VA_ARGS__)))(__VA_ARGS__)
#define LIST_ISPARENTHETICAL_1(x) ISPARENTHETICAL(x)
#define LIST_ISPARENTHETICAL_0(...) __VA_ARGS__

/**
 * Retreives the indexed element of the list.
 *
 * @param n - The index of the list element to retrieve.
 * @param ... - The list to retrieve the nth element from.
 *
 * @return The element of the list found at position n.
 */
#define LIST_SELECT(n, ...) FIRST(LIST_BACK(n, __VA_ARGS__))

/**
 * Safely joins two lists into a single continuous list.
 *
 * @param list1 - A parenthetically encapsulated list of 0 or more elements to
 *                form the front of the new list. If the list contains 0 or 1
 *                elements, the parentheses aren't necessary.
 * @param list2 - A parenthetically encapsualted list of 0 or more elements to
 *                form the back of the new list. If the list contains 0 or 1
 *                elements, the parentheses aren't necessary.
 *
 * @return A non-parenthetically encapsulated list consisting of the contents
 *         of list1 followed by the contents of list2. If either list1 or list2
 *         is empty, the empty components will not be included in the result.
 */
#define LIST_JOIN(list1, list2) PASTE3(LIST_JOIN, IIF(LIST_ISPARENTHETICAL(list1))(ISTRUE(LIST_LENGTH list1), LIST_LENGTH(list1)), IIF(LIST_ISPARENTHETICAL(list2))(ISTRUE(LIST_LENGTH list2), LIST_LENGTH(list2)))(list1, list2)
#define LIST_JOIN00(list1, list2)
#define LIST_JOIN01(list1, list2) LIST_DECAPSULATE(list2)
#define LIST_JOIN10(list1, list2) LIST_DECAPSULATE(list1)
#define LIST_JOIN11(list1, list2) LIST_DECAPSULATE(list1), LIST_DECAPSULATE(list2)

/**
 * Inserts the supplied element at the specified position in the list.
 *
 * @param n   - The index to insert element `a` into in the supplied list.
 * @param a   - The element to insert into the list at position `n`.
 * @param ... - The list to insert the element a at position n into.
 *
 * @return A non-parenthetically encapsulated list formed from the input list
 *         with the specified separator token. 
 */
#define LIST_INSERT(n, a, ...) LIST_JOIN((LIST_JOIN((LIST_FRONT(n, __VA_ARGS__)), (a))), (LIST_BACK(n, __VA_ARGS__)))

/**
 * eats the supplied list.
 */
#define LIST_EAT(...)

/**
 * Does nothing. simply returns its argument list unaltered. This is useful for
 * list processing operations where the argument list needs to be preserved, or
 * the elements only need to be swapped.
 */
#define LIST_PASS(...) __VA_ARGS__

/**
 * Determines the length of the supplied list. Any sublists that are
 * parenthetically encapsulated will be considered a single element.
 * 
 * @note Works for list consisting of 0 to 128 elements. The result is undefined
 *       for lists with more than 128 elements.
 *
 * @param ... - The list to measure.
 *
 * @return The number of elements found in the list.
 */
#define LIST_LENGTH(...) LIST_LENGTH_EXPAND(DEFER(1, LIST_LENGTH_)(__VA_ARGS__, LIST_LENGTH_TAPEMEASURE_, ~))
#define LIST_LENGTH_EXPAND(...) __VA_ARGS__
#define LIST_LENGTH_TAPEMEASURE_ 128, 127, 126, 125, 124, 123, 122, 121, 120, 119, 118, 117, 116, 115, 114, 113, 112, 111, 110, 109, 108, 107, 106, 105, 104, 103, 102, 101, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1
#define LIST_LENGTH_(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, a51, a52, a53, a54, a55, a56, a57, a58, a59, a60, a61, a62, a63, a64, a65, a66, a67, a68, a69, a70, a71, a72, a73, a74, a75, a76, a77, a78, a79, a80, a81, a82, a83, a84, a85, a86, a87, a88, a89, a90, a91, a92, a93, a94, a95, a96, a97, a98, a99, a100, a101, a102, a103, a104, a105, a106, a107, a108, a109, a110, a111, a112, a113, a114, a115, a116, a117, a118, a119, a120, a121, a122, a123, a124, a125, a126, a127, a128, ...) IIF(AND(ISONE_E(a128), ISBLANK(a0)))(0, a128)

/// \todo use ISEQUAL instead? or is this too big of a dependency? 
#define ISONE_E(x) ISONE_EXPAND(DEFER(1, SECOND)(PASTE(ISONE_E, x), 0))
#define ISONE_EXPAND(...) __VA_ARGS__
#define ISONE_E1 ~, 1


#endif /*CPP_LIST_UTIL_H */

