/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * This file contains macros implementing a list filter operator.
 *
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_LIST_FILTER_H
#define CPP_LIST_FILTER_H

#include "cpp/util.h"
#include "cpp/placeholder.h"
#include "cpp/list/util.h"
#include "cpp/logic.h"
#include "cpp/integer.h"
#include "cpp/expand.h"

/**
 * Filters the supplied list by matching each element against the given
 * condition. This facility supports placeholder argument expansion in the
 * condition argument list.
 *
 * @param cond - The condition for included elements to meet. Must be a
 *               function-like macro.
 * @param args - The argument list for the cond macro. Each element of the list
 *               will be mapped to the _1 placeholder in turn. level will be
 *               mapped to _2.
 * @param sep  - The separator character to be used between included elements.
 *               Must be a function-like macro. 
 * @param ...  - The list to be filtered.
 *
 * @return The input list with all elements that did not match the specified
 *         condition removed.
 */
#define LIST_FILTER(cond, args, sep, ...) LIST_FILTERL(0, cond, args, sep, __VA_ARGS__)

#define LIST_FILTERL(level, cond, args, sep, ...) LIST_FILTERLE(EXPAND, level, cond, args, sep, __VA_ARGS__)

#define LIST_FILTERLE(expand, level, cond, args, sep, ...) LIST_FILTERL_I(expand, level, cond, args, sep, LIST_DECAPSULATE(__VA_ARGS__))

#define LIST_FILTERL_I(expand, level, cond, args, sep, ...) expand(level)(0)(LIST_FILTER_I(expand, level, LIST_LENGTH(__VA_ARGS__), cond, LIST_LENGTH args, args, 0, sep, __VA_ARGS__, ~))

#define LIST_FILTER_I(expand, level, state, cond, nargs, args, element, sep, ...) IIF(state)(LIST_FILTER_NEXT, LIST_EAT)(expand, level, state, cond, nargs, args, element, sep, __VA_ARGS__)

#define LIST_FILTER_INDIRECT() LIST_FILTER_I

#define LIST_FILTER_NEXT(expand, level, state, cond, nargs, args, element, sep, a1, ...) LIST_FILTER_NEXT_I(expand, level, state, cond, nargs, args, element, sep, PLACEHOLDER_EVALM(cond, nargs, (a1, element, NEXT(level), ~), args), a1, __VA_ARGS__)

#define LIST_FILTER_NEXT_I(expand, level, state, cond, nargs, args, element, sep, cond_a1, a1, ...) IIF(cond_a1)(IIF(element)(sep(),)a1,)DEFER(1, expand(level)(state))(DEFER(1, LIST_FILTER_INDIRECT)()(expand, level, PREV(state), cond, nargs, args, IIF(cond_a1)(NEXT(element), element), sep, __VA_ARGS__))

#endif /* CPP_LIST_FILTER_H */

