/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * This file contains the implementation of a list mapping macro, allowing
 * lists to be transformed one element at a time by the repeated application
 * of a user supplied macro.
 *
 * @author Matt Voroney
 */
#pragma once
#ifndef CPP_LIST_MAP_H
#define CPP_LIST_MAP_H

#include "cpp/util.h"
#include "cpp/logic.h"
#include "cpp/list/util.h"
#include "cpp/placeholder.h"
#include "cpp/integer.h"

#ifndef NO_DEFAULT_EXPANDER
#   include "cpp/expand.h"
#endif

/**
 * Maps the input list to a list of the same size, with the results of applying
 * the supplied macro to each element of the input list.
 * 
 * @param macro - the macro to apply to each element of the list.
 * @param args  - a parentheses encapsulated list of arguments to be passed to
 *                the supplied macro. Placeholder expansion will be performed as
 *                follows:
 *                  - _1 -The current element of the list being processed.
 *                  - _2 - The nesting level the macro was invoked at.
 * @param sep   - The separator to place between mapped list elements. Expected
 *                to be a no-arg macro.
 *
 * @return A list containing the results of applying the supplied macro to each
 *         of the elements of the list, according to the supplied args specification.
 *
 * @see LIST_MAPL() A variant of this macro capable of being nested up to n levels
 *      deep, where 'n' is the number of currently defined recursive back end
 *      expanders.
 * @see EXPAND() For more information on the recursive back end expansion facility.
 * @see cpp/util.h For some common separator token macros.
 */
#define LIST_MAP(macro, args, sep, ...) LIST_MAPL(0, macro, args, sep, __VA_ARGS__)
#define LIST_MAP0(macro, args, sep, ...) LIST_MAPL(0, macro, args, sep, __VA_ARGS__)
#define LIST_MAP1(macro, args, sep, ...) LIST_MAPL(1, macro, args, sep, __VA_ARGS__)
#define LIST_MAP2(macro, args, sep, ...) LIST_MAPL(2, macro, args, sep, __VA_ARGS__)
#define LIST_MAP3(macro, args, sep, ...) LIST_MAPL(3, macro, args, sep, __VA_ARGS__)
#define LIST_MAP4(macro, args, sep, ...) LIST_MAPL(4, macro, args, sep, __VA_ARGS__)
#define LIST_MAP5(macro, args, sep, ...) LIST_MAPL(5, macro, args, sep, __VA_ARGS__)

/**
 * A variant of LIST_MAP() that allows for nesting of itself, or any other iterative
 * L variant repetition macro. During placeholder expansion of the macro arguments,
 * `_1` will be defined as the current list element, and _2 will be defined as the
 * nesting level to be used for iterative L variant repetition macros.
 *
 * @param level - The nesting level to assume for the recursive back end used by
 *                this macro. Valid values for level depend on what levels
 *                EXPAND() has been implemented for. By default this is [0, 5].
 * @param macro - The macro to apply to each element of the list. The result of the
 *                macro will be the new value of the corresponding element in the
 *                resultant list.
 * @param args  - A parentheses encapsulated argument list to be passed to the
 *                supplied macro. Placeholder expansion will be performed as
 *                follows:
 *                  - _1 -The current element of the list being processed.
 *                  - _2 - The nesting level the macro was invoked at.
 * @param sep   - The separator token to be placed between derived list elements.
 *                Expected to be a no-arg macro.
 * @param ...   - The list to perform the mapping on.
 *
 * @return A list containing the results of applying the supplied macro to each
 *         of the elements of the list, according to the supplied args specification.
 *
 * @see LIST_MAP() A variant of this macro which cannot be safely nested, but is
 *      slightly simpler to use where nesting is not required. 
 * @see EXPAND() For more information on the recursive back end expansion facility.
 * @see cpp/util.h For some common separator token macros.
 */
#define LIST_MAPL(level, macro, args, sep, ...) LIST_MAPLE(EXPAND, level, macro, args, sep, __VA_ARGS__)

/**
 * A list map variant allowing the use of a user defined iterative expander.
 *
 * @param expand - The name of the macro expander to use.
 * 
 * @see LIST_MAPL() For an explanation of the remaining parameters. This macro
 *  is identical except that it takes an additional expand parameter in the 
 *  first slot.
 * @see EXPAND() For an example illustrating the expected behavior for expand
 */
#define LIST_MAPLE(expand, level, macro, args, sep, ...) LIST_MAPL_I(expand, level, macro, args, sep, LIST_DECAPSULATE(__VA_ARGS__))

/// @privatesection
/**
 * Internal stage 1 driver. Calculates starting state and various argument list lengths.
 */
#define LIST_MAPL_I(expand, level, macro, args, sep, ...) LIST_MAPL_II(expand, level, LIST_LENGTH(__VA_ARGS__), macro, LIST_LENGTH args, args, sep, __VA_ARGS__, ~)

/**
 * checks to see if the supplied list was empty or not.
 */
#define LIST_MAPL_II(expand, level, state, macro, nargs, args, sep, ...) IIF(state)(LIST_MAPL_III, LIST_EAT)(expand, level, state, macro, nargs, args, sep, __VA_ARGS__)

/**
 * Internal stage 2 driver. Performs first element processing separate
 * from the rest of the list so that separator tokens will be correctly
 * inserted into the resultant list. Also precalculates values that are
 * invariant for the entire iterative process, or that will simplify things.
 */
#define LIST_MAPL_III(expand, level, state, macro, nargs, args, sep, a1, ...) PLACEHOLDER_EVALM(macro, nargs, (a1, NEXT(level), ~), args)expand(level)(0)(LIST_MAP_I(expand, level, PREV(state), macro, nargs, args, sep, __VA_ARGS__))

/**
 * Internal Stage 2 driver. Decides whether to proceed to the next iteration,
 * or to terminate the iteration.
 */
#define LIST_MAP_I(expand, level, state, macro, nargs, args, sep, ...) IIF(state)(LIST_MAP_NEXT, LIST_EAT)(expand, level, state, macro, nargs, args, sep, __VA_ARGS__)

/**
 * Acts as a bridge to decouple expansions of LIST_MAP_I() and LIST_MAP_NEXT().
 * In order for the iteration to work, one of the two macros cannot expand to
 * the other in the same phase of expansion, otherwise the preprocessor will
 * mark that token as already expanded, and the iteration will stop there.
 */
#define LIST_MAP_INDIRECT() LIST_MAP_I

/**
 * Calculates the current list element, and sets up the next iteration.
 */
#define LIST_MAP_NEXT(expand, level, state, macro, nargs, args, sep, a1, ...) sep()PLACEHOLDER_EVALM(macro, nargs, (a1, NEXT(level), ~), args)DEFER(1, expand(level)(state))(DEFER(1, LIST_MAP_INDIRECT)() (expand, level, PREV(state), macro, nargs, args, sep, __VA_ARGS__))


#endif /* CPP_LIST_MAP_H */

