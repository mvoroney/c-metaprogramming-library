/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef TYPE_CONDITION_H
#define TYPE_CONDITION_H

#include "type/forward.h"

namespace type {

    namespace condition {

        template<
            bool, 
            typename T, 
            typename F, 
            template<typename> class HT, 
            template<typename> class HF>
        struct expand {
            typedef typename HF<F>::type type;
        };

        template<
            typename T, 
            template<typename> class HT, 
            template<typename> class HF>
        struct expand<false, T, type::none, HT, HF> {};
        
        template<
            typename T, 
            typename F, 
            template<typename> class HT, 
            template<typename> class HF>
        struct expand<true, T, F, HT, HF> {
            typedef typename HT<T>::type type;
        };
    }
}

#endif /* TYPE_CONDITION_H */

