/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef TYPE_CHECK_H
#define TYPE_CHECK_H

#include "type/forward.h"
#include "type/condition.h"

namespace type {

    namespace has {

        /**
         * Usage:
         *
         * \code{.cpp}
         * WITH_TYPEDEF(value_type);
         * has_typedef<std::vector<int>, with_value_type>::value;
         * \endcode
         */
        template<typename T, typename H>
        struct member : private H {
            using H::check;

            template<typename U> static type::false_t check(...);

            static const bool value = sizeof(member<T, H>::template check<T>(0)) == sizeof(type::true_t);
        };
    }
}

#define WITH_TYPEDEF( typedef_name ) \
struct with_##typedef_name { template<typename U> static type::true_t check(typename U:: typedef_name *); }

#endif /* TYPE_CHECK_H */

