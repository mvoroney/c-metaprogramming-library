/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef TYPE_FORWARD_H
#define TYPE_FORWARD_H

namespace type {

    struct true_t;
    struct false_t;
    struct none {};
    
    template<typename T, typename U>
    struct eq;

    template<typename T>
    struct forward;

    namespace remove {

        template<typename T> struct sign;
    }

    namespace condition {

        template<
            bool, 
            typename T, 
            typename F = type::none, 
            template<typename> class HT = type::forward, 
            template<typename> class HF = type::forward>
        struct expand;
    }

    namespace has {
        
        template<typename T, typename H> struct member;
    }
}

#endif /* TYPE_FORWARD_H */

