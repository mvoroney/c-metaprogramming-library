/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef TYPE_MANIP_H
#define TYPE_MANIP_H

namespace type {

    namespace remove {

        template<typename T>
        struct sign {
            typedef T type;
        };
        
        template<>
        struct sign<char> {
            typedef unsigned char type;
        };

        template<>
        struct sign<signed char> {
            typedef unsigned char type;
        };
        
        template<>
        struct sign<signed short int> {
            typedef unsigned short int type;
        };
        
        template<>
        struct sign<signed int> {
            typedef unsigned int type;
        };
        
        template<>
        struct sign<signed long int> {
            typedef unsigned long int type;
        };
        
        template<>
        struct sign<signed long long int> {
            typedef unsigned long long int type;
        };
    }
}

#endif /* TYPE_MANIP_H */

