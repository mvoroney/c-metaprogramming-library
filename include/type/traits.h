/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef TYPE_TRAITS_H
#define TYPE_TRAITS_H

#include "type/check.h"

#define FORWARD_TYPE( typedef_name ) \
template<typename T__> struct forward_##typedef_name { typedef typename T__:: typedef_name type; }

namespace type {

    template<typename T>
    struct traits {
      private:
        WITH_TYPEDEF(value_type);
        FORWARD_TYPE(value_type);
      public:

        static const bool value = type::has::member<
            T, with_value_type
        >::value;
        
        typedef typename type::condition::expand<
            value, 
            T, 
            T, 
            forward_value_type
        >::type value_type;
    };

}

#endif /* TYPE_TRAITS_H */
