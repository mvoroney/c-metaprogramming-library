/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifndef TYPE_UTIL_H
#define TYPE_UTIL_H

namespace type {
    struct true_t {
        static const bool value = true;
    private:
        char a[2];    
    };
    
    struct false_t {
        static const bool value = false;
    private:
        char a[1];
    };
   
    template<typename T, typename U>
    struct eq {
        static const bool value = false;
        typedef false_t type;
    };
    
    template<typename T>
    struct eq<T, T> {
        static const bool value = true;
        typedef true_t type;
    };

    template<typename T>
    struct forward {
        typedef T type;
    };
}

#endif /* TYPE_UTIL_H */

