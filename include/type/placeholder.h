/*
 * CPP Meta - A metaprogramming library for c++. 
 * 
 * Copyright (C) 2017 Matt Voroney matt.voroney@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TYPE_LAMBDA_PLACEHOLDER_H
#define TYPE_LAMBDA_PLACEHOLDER_H

#define T_ARGS( count, basename, default_type ) \
    T_ARGS##count( basename, default_type )

#define T_ARGS1( b, d ) typename b##1 = d
#define T_ARGS2( b, d ) T_ARGS1(b, d), typename b##2 = d
#define T_ARGS3( b, d ) T_ARGS2(b, d), typename b##3 = d
#define T_ARGS4( b, d ) T_ARGS3(b, d), typename b##4 = d
#define T_ARGS5( b, d ) T_ARGS4(b, d), typename b##5 = d

namespace type {

    namespace lambda {
    
        namespace placeholder {

            struct empty {};
            template<int> struct arg {};
            
            template<>
            struct arg<1> {
                template<T_ARGS(5, A, empty)>
                struct apply { typedef A1 type; };
            };
            
            template<>
            struct arg<2> {
                template<T_ARGS(5, A, empty)>
                struct apply { typedef A2 type; };
            };
            
            template<>
            struct arg<3> {
                template<T_ARGS(5, A, empty)>
                struct apply { typedef A3 type; };
            };
            
            template<>
            struct arg<4> {
                template<T_ARGS(5, A, empty)>
                struct apply { typedef A4 type; };
            };
            
            template<>
            struct arg<5> {
                template<T_ARGS(5, A, empty)>
                struct apply { typedef A5 type; };
            };
            
            typedef arg<1> _1;
            typedef arg<2> _2;
            typedef arg<3> _3;
            typedef arg<4> _4;
            typedef arg<5> _5;
        }
    }
}

#endif /* TYPE_LAMBDA_PLACEHOLDER_H */

